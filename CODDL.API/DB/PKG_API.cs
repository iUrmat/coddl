/* Filename for save = PKG_API.cs
*  IvoCodeGenerator, 22.10.2015 10:06:34
*/
using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;

using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

using System.Collections.Generic;
using CODDL.API.Model;

namespace CODDL.API.DB
{
    public class PKG_API
    {
        
        public OracleConnection Connertor { get; set; }
        public  int CurrentRecNo { get; set; }
        public int MaxRecNo { get; set; }
        public PKG_API(OracleConnection conn)
        {
            Connertor = conn;
        }
        public virtual DataTable GETOBJECTTYPELIST(
            )
        {
            using (OracleCommand cmd = GetOracleProcCommand("CODDL.PKG_API.GETOBJECTTYPELIST"))
            {
                OracleParameter par_OUTP = new OracleParameter();
                par_OUTP.OracleDbType = OracleDbType.RefCursor;
                par_OUTP.Direction = ParameterDirection.ReturnValue;
                par_OUTP.ParameterName = "OUTP";
                cmd.Parameters.Add(par_OUTP);
                cmd.FetchSize = 35000000; // подобрано тестами
                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                {
                    DataTable dt = new DataTable(); da.Fill(dt);
                    par_OUTP.Dispose();
                    return dt;
                }
            }
        }
        public virtual DataTable GETOWNERLIST(
            )
        {
            using (OracleCommand cmd =  GetOracleProcCommand("CODDL.PKG_API.GETOWNERLIST"))
            {
                OracleParameter par_OUTP = new OracleParameter();
                par_OUTP.OracleDbType = OracleDbType.RefCursor;
                par_OUTP.Direction = ParameterDirection.ReturnValue;
                par_OUTP.ParameterName = "OUTP";
                cmd.Parameters.Add(par_OUTP);
                cmd.FetchSize = 35000000; // подобрано тестами
                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                {
                    DataTable dt = new DataTable(); da.Fill(dt);
                    par_OUTP.Dispose();
                    return dt;
                }
            }
        }
        public virtual DataTable GETOWNERLIST_BYFLT(
             string PSCHEMAWILDCARDCSV
           )
        {
            using (OracleCommand cmd = GetOracleProcCommand("CODDL.PKG_API.GETOWNERLIST_BYFLT"))
            {
                OracleParameter par_OUTP = new OracleParameter();
                par_OUTP.OracleDbType = OracleDbType.RefCursor;
                par_OUTP.Direction = ParameterDirection.ReturnValue;
                par_OUTP.ParameterName = "OUTP";
                cmd.Parameters.Add(par_OUTP);
                OracleParameter par_PSCHEMAWILDCARDCSV = new OracleParameter();
                par_PSCHEMAWILDCARDCSV.OracleDbType = OracleDbType.Varchar2;
                par_PSCHEMAWILDCARDCSV.Direction = ParameterDirection.Input;
                par_PSCHEMAWILDCARDCSV.ParameterName = "PSCHEMAWILDCARDCSV";
                cmd.Parameters.Add(par_PSCHEMAWILDCARDCSV);
                par_PSCHEMAWILDCARDCSV.Size = 2000;
                par_PSCHEMAWILDCARDCSV.Value = PSCHEMAWILDCARDCSV;
                cmd.FetchSize = 35000000; // подобрано тестами
                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                {
                    DataTable dt = new DataTable(); da.Fill(dt);
                    par_OUTP.Dispose();
                    par_PSCHEMAWILDCARDCSV.Dispose();
                    return dt;
                }
            }
        }

        public virtual DataTable GETOBJECTLIST(
              string POBJECTTYPESCSV
              , string POWNERSCSV
              , string POBJECTNAMELIKEFILTER
            )
        {
            using (OracleCommand cmd = GetOracleProcCommand("CODDL.PKG_API.GETOBJECTLIST"))
            {
                OracleParameter par_OUTP = new OracleParameter();
                par_OUTP.OracleDbType = OracleDbType.RefCursor;
                par_OUTP.Direction = ParameterDirection.ReturnValue;
                par_OUTP.ParameterName = "OUTP";
                cmd.Parameters.Add(par_OUTP);
                OracleParameter par_POBJECTTYPESCSV = new OracleParameter();
                par_POBJECTTYPESCSV.OracleDbType = OracleDbType.Varchar2;
                par_POBJECTTYPESCSV.Direction = ParameterDirection.Input;
                par_POBJECTTYPESCSV.ParameterName = "POBJECTTYPESCSV";
                cmd.Parameters.Add(par_POBJECTTYPESCSV);
                par_POBJECTTYPESCSV.Size = 2000;
                OracleParameter par_POWNERSCSV = new OracleParameter();
                par_POWNERSCSV.OracleDbType = OracleDbType.Varchar2;
                par_POWNERSCSV.Direction = ParameterDirection.Input;
                par_POWNERSCSV.ParameterName = "POWNERSCSV";
                cmd.Parameters.Add(par_POWNERSCSV);
                par_POWNERSCSV.Size = 2000;
                OracleParameter par_POBJECTNAMELIKEFILTER = new OracleParameter();
                par_POBJECTNAMELIKEFILTER.OracleDbType = OracleDbType.Varchar2;
                par_POBJECTNAMELIKEFILTER.Direction = ParameterDirection.Input;
                par_POBJECTNAMELIKEFILTER.ParameterName = "POBJECTNAMELIKEFILTER";
                cmd.Parameters.Add(par_POBJECTNAMELIKEFILTER);
                par_POBJECTNAMELIKEFILTER.Size = 2000;
                par_POBJECTTYPESCSV.Value = POBJECTTYPESCSV;
                par_POWNERSCSV.Value = POWNERSCSV;
                par_POBJECTNAMELIKEFILTER.Value = POBJECTNAMELIKEFILTER;
                cmd.FetchSize = 35000000; // подобрано тестами
                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                {
                    DataTable dt = new DataTable(); da.Fill(dt);
                    par_OUTP.Dispose();
                    par_POBJECTNAMELIKEFILTER.Dispose();
                    par_POBJECTTYPESCSV.Dispose();
                    par_POWNERSCSV.Dispose();
                    return dt;
                }
            }
        }
        /// <summary>
        /// Get DDL object by filter 
        /// </summary>
        /// <param name="POBJECTTYPESCSV">type list</param>
        /// <param name="POWNERSCSV">owner list </param>
        /// <param name="POBJECTNAMELIKEFILTER"></param>
        /// <returns></returns>
        public virtual List<DDLObject> GETDDLS_FOROBJECTLIST(
              string POBJECTTYPESCSV
              , string POWNERSCSV
              , string POBJECTNAMELIKEFILTER
              
            )

        {
            CurrentRecNo = 0;
            MaxRecNo = 0;
            using (OracleCommand cmd = GetOracleProcCommand("CODDL.PKG_API.GETDDLS_FOROBJECTLIST"))
            {
                OracleParameter par_OUTP = new OracleParameter();
                par_OUTP.OracleDbType = OracleDbType.RefCursor;
                par_OUTP.Direction = ParameterDirection.ReturnValue;
                par_OUTP.ParameterName = "OUTP";
                cmd.Parameters.Add(par_OUTP);
                OracleParameter par_POBJECTTYPESCSV = new OracleParameter();
                par_POBJECTTYPESCSV.OracleDbType = OracleDbType.Varchar2;
                par_POBJECTTYPESCSV.Direction = ParameterDirection.Input;
                par_POBJECTTYPESCSV.ParameterName = "POBJECTTYPESCSV";
                cmd.Parameters.Add(par_POBJECTTYPESCSV);
                par_POBJECTTYPESCSV.Size = 2000;
                OracleParameter par_POWNERSCSV = new OracleParameter();
                par_POWNERSCSV.OracleDbType = OracleDbType.Varchar2;
                par_POWNERSCSV.Direction = ParameterDirection.Input;
                par_POWNERSCSV.ParameterName = "POWNERSCSV";
                cmd.Parameters.Add(par_POWNERSCSV);
                par_POWNERSCSV.Size = 2000;
                OracleParameter par_POBJECTNAMELIKEFILTER = new OracleParameter();
                par_POBJECTNAMELIKEFILTER.OracleDbType = OracleDbType.Varchar2;
                par_POBJECTNAMELIKEFILTER.Direction = ParameterDirection.Input;
                par_POBJECTNAMELIKEFILTER.ParameterName = "POBJECTNAMELIKEFILTER";
                cmd.Parameters.Add(par_POBJECTNAMELIKEFILTER);
                par_POBJECTNAMELIKEFILTER.Size = 2000;
                par_POBJECTTYPESCSV.Value = POBJECTTYPESCSV;
                par_POWNERSCSV.Value = POWNERSCSV;
                par_POBJECTNAMELIKEFILTER.Value = POBJECTNAMELIKEFILTER;
                cmd.FetchSize = 3500; // подобрано тестами

                List<DDLObject> Result = new List<DDLObject>();
                try
                {
                    using (OracleDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            DDLObject ddlObject = new DDLObject(owner: reader["OWNER"] as string,
                                                       ddltype: reader["OBJECTTYPE"] as string,
                                                        ddlName: reader["OBJECTNAME"] as string,
                                                        ddltext: reader["DDLTEXT"] as string);
                            Result.Add(ddlObject);
                            try
                            {
                                var val = (reader["ObjCount"] as decimal?);
                                MaxRecNo = val.HasValue ? (int)val.Value : 0;
                            }
                            catch
                            {
                                MaxRecNo = 100;
                            }
                            try
                            {
                                var val = (reader["ObjNo"] as decimal?);
                                CurrentRecNo = val.HasValue ? (int)val.Value : 0;
                            }
                            catch
                            {
                                CurrentRecNo++;
                            }
                            Debug.WriteLine("Max:" + MaxRecNo + " current: " + CurrentRecNo);

                            
                        }

                    }
                }
                finally
                {
                    par_OUTP.Dispose();
                    par_POBJECTNAMELIKEFILTER.Dispose();
                    par_POBJECTTYPESCSV.Dispose();
                    par_POWNERSCSV.Dispose();
                }
                return Result;
                
            }
        }
      

        private OracleCommand GetOracleProcCommand( string text)
        {
            OracleCommand outp = Connertor.CreateCommand();
            outp.CommandType = System.Data.CommandType.StoredProcedure;
            outp.CommandText = text;
            return outp;
        }
    }
}
          


