﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODDL.API.DB
{
    /// <summary>
    /// Oracle connect
    /// </summary>
    public sealed class OraConnector
    {
        private static readonly Lazy<OraConnector> _instance = new Lazy<OraConnector>(() => new OraConnector());
        //скрываем конструктор
        OraConnector() { }
        public static OraConnector Instance { get { return _instance.Value; } }
        private OracleConnection _conn;
        private string _username;
        private string _datasource;

        public OracleConnection Conn
        {
            get
            {
                if (_conn == null)
                {
                    throw new Exception("Нет соединения с БД");
                }

                return _conn;
            }
        }
        /// <summary>
        /// Return true if connection state on open
        /// </summary>
        public bool IsConnected { get { return _conn != null && _conn.State == System.Data.ConnectionState.Open; } }


        public string ConnectionInfo
        {
            get
            {
                if (_conn == null || _conn.State != System.Data.ConnectionState.Open) { return "No database connection!"; }
                else
                {
                    return String.Format("Connected  {0}@{1}", _username, _datasource);
                }
            }
        }

        /// <summary>
        /// Connect to database
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="Scheme"></param>
        /// <returns></returns>
        public OracleConnection connect(string username, string password, string Scheme= "krdev12")
        {
            string oradb = "Data Source=" + Scheme + ";User Id=" + username + ";Password=" + password + ";";



            _conn = new OracleConnection(oradb);
            

            _conn.Open();

            
            _username = username;
            _datasource = Scheme;
            return _conn;
        }
        /// <summary>
        /// Connect to database with connection string
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="scheme"></param>
        /// <returns></returns>
        public OracleConnection connectstr(string username, string password,string scheme= "krhot")
        {
            string connstring =
                       "Data Source=(DESCRIPTION=" +
                                                         "(ADDRESS_LIST =" +
                                                               "(ADDRESS =" +
                                                                  "(COMMUNITY = "+ scheme + ".karavay.spb.ru)" +
                                                                  "(PROTOCOL = TCP)" +
                                                                  "(HOST = SERV06DB)" +
                                                                  "(PORT = 1521)" +
                                                               ")" +
                                                          ")" +
                                                          "(CONNECT_DATA = (SID = KRMAIN)" +
                                                          ")" +
                                                        "); User Id=" + username + ";Password=" + password + ";";
            return connect(connstring);
    }
        /// <summary>
        /// Connect to database with connection string 
        /// </summary>
        /// <param name="constr">Connection string </param>
        /// <returns></returns>
        public OracleConnection connect(string constr )
        {
            //string connstring =
            //            "Data Source=(DESCRIPTION=(ADDRESS=(COMMUNITY = tcp.karavay.spb.ru)(PROTOCOL=TCP)(HOST=serv11db)(PORT=1521))" +
            //            "(CONNECT_DATA=(SID = KRMAIN)));User Id=" + username + ";Password=" + password + ";";

            _conn = new OracleConnection(constr);

            _conn.Open();
            

            return _conn;
        }
        /// <summary>
        /// Create oracle command 
        /// </summary>
        /// <param name="text">procedure name </param>
        /// <returns>OracleCommand</returns>
        public OracleCommand GetOracleProcCommand(string text)
        {
            OracleCommand outp = _conn.CreateCommand();
            outp.CommandType = System.Data.CommandType.StoredProcedure;
            outp.CommandText = text;
            return outp;
        }
        /// <summary>
        /// Close connection if open
        /// </summary>
        public  void CloseConnection()
        {
            if (_conn != null && _conn.State != System.Data.ConnectionState.Closed)
            {
                _conn.Dispose();
                _conn = null;
            }
        }
    }
    
}
