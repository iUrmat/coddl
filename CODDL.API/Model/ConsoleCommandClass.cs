﻿using CODDL.API.Model;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CODDL.API
{
    /// <summary>
    /// List of Commands
    /// </summary>
    [Serializable]
    public class ConsoleCommand
    {
     
        public List<BundleCommand> Commands { get; }
        public ConsoleCommand()
        {
            Commands = new List<BundleCommand>();
        }

       
    }
    public abstract class Cloneble:ICloneable{
      
          public object Clone()
            {
                return this.MemberwiseClone();
            }
        }
    /// <summary>
    /// Class for configuration the load operation
    /// </summary>
   [Serializable]
    public class Retrieve: Cloneble
    {
        /// <summary>
        /// Load from 0-from DB, 1-from 
        /// </summary>
        public BundleType RetrieveType { get; set; }

        public string BundleFrom { get; set; } //For from DB: ServerName 
        public string Login { get; set; }
        public string Password { get; set; }



        /// <summary>
        /// Strings with separator ','
        /// </summary>
        public List<string> ObjectTypes { get; set; }
        /// <summary>
        /// string with separator ','
        /// </summary>
        public List<string> ObjectOwners { get; set; }
        public string ObjectNameFilter { get; set; }

        public Retrieve() {
            ObjectTypes = new List<string>();
            ObjectOwners = new List<string>();
        }
    }
    /// <summary>
    /// class for configuration the save operation
    /// </summary>
    [Serializable]
    public class Save: Cloneble
    {
        /// <summary>
        /// char encoding
        /// </summary>
        public string Encoding { get; set; }
        /// <summary>
        /// user name for git
        /// </summary>
        public string GitUserName { get; set; }
        /// <summary>
        /// user email for git
        /// </summary>
        public string GitUserEmail { get; set; }
        /// <summary>
        /// user password for git
        /// </summary>
        public string GitUserPass { get; set; }
        /// <summary>
        /// Save type (GIT or Directory)
        /// </summary>
        public BundleType SaveType { get; set; }

        //  public string OutputPath { get; set; }
        /// <summary>
        /// Root path for save
        /// </summary>
        public string OutputPath { get; set; }
        public Save() {

            OutputPath = @"d:\temp\";
            Encoding = "UTF-8";
        }
    }
    /// <summary>
    /// Bundle command
    /// </summary>
    [Serializable]
    public class BundleCommand : Cloneble
    {
          
        public Retrieve Retrieve { get; set; }
        public Save Save { get; set; }
        public string LogFileName { get; set; }
        public BundleCommand()
        {
            Retrieve = new Retrieve();
            Save = new Save();
            LogFileName = "BundleConsole.Log";
        }
        public new object  Clone()
        {
            Retrieve tmpRet = Retrieve.Clone() as Retrieve;
            Save tmpsave = Save.Clone() as Save;
            var tmp = this.MemberwiseClone() as BundleCommand;
            tmp.Retrieve = tmpRet;
            tmp.Save = tmpsave;
            return tmp;
        }
        

      
    }
}
