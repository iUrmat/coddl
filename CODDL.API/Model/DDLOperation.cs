﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CODDL.API.Model
{
    /// <summary>
    /// Operation for DDL bundle, run save or load commands
    /// </summary>
    public class DDLOperation : IDisposable
    {
        /// <summary>
        /// for async run
        /// </summary>
        BackgroundWorker bw;

/// <summary>
///  
/// </summary>
        IProgressable Operation;

        /// <summary>
        /// Event for progress changed
        /// </summary>
        public event EventHandler<ProgressChangedEventArgsWithMax> OnProgressChanged;
        /// <summary>
        /// Event load completed
        /// </summary>
        public event EventHandler<RunWorkerCompletedEventArgs> OnLoadCompleted;

        DDLBundle _bunlde { get; set; }
        public DDLOperation(DDLBundle bunlde)
        {
            _bunlde = bunlde;
        }
        /// <summary>
        /// Start operation IProgressable
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="RunSynchronous">if false start operation in  BackgroundWorker(Synchronous) </param>
        public void Start(IProgressable operation, bool RunSynchronous = false)
        {

            if (operation == null)
                throw new Exception("Operation should not by empty!!!");
            Operation = operation;
            if (RunSynchronous)
            {
                List<DDLObject> res = Operation.Run(_bunlde) as List<DDLObject>;
                if (res != null)
                {
                    _bunlde.DDLObjects.Clear();
                    _bunlde.DDLObjects.AddRange(res);
                }
                return;
            }

            
            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;


            bw.DoWork += (s, e) =>
            {
                
                e.Result = Operation.Run(_bunlde);//  retrive.Load(BundleInfo);
            };


            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;

            bw.ProgressChanged += Bw_ProgressChanged;



            bw.RunWorkerAsync();

            //Feed to obtain the state of progress
            Thread LoaderProgress = GetCurrenProgressThread(operation, bw);
            LoaderProgress.Start();

        }

        private void Bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Debug.WriteLine(e.ProgressPercentage.ToString());
            ProgressChangedEventArgsWithMax arg = new ProgressChangedEventArgsWithMax(e.ProgressPercentage, e.UserState);
            arg.MaxProgress = Operation.MaxProgress;
            RunProgressChanged(arg);
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            List<DDLObject> res = e.Result as List<DDLObject>;
            if (res != null)
            {
                _bunlde.DDLObjects.Clear();
                _bunlde.DDLObjects.AddRange(res);
                int count = res.Count;
                ProgressChangedEventArgsWithMax arg = new ProgressChangedEventArgsWithMax(count, e.UserState);
                arg.MaxProgress = res.Count;
                RunProgressChanged(arg);
            }
            if (OnLoadCompleted != null)
                OnLoadCompleted(this, e);
        }

        private void RunProgressChanged(ProgressChangedEventArgsWithMax arg)
        {

            if (OnProgressChanged != null)
            {
                OnProgressChanged(this, arg);
            }
        }



        /// <summary>
        /// Thread for get current progress value
        /// </summary>
        /// <returns></returns>
        private Thread GetCurrenProgressThread(IProgressable progressableObject, BackgroundWorker bworker)
        {
            Thread LoaderProgress = new Thread(delegate ()
            {

                RunGetCurrentProgress(progressableObject, bworker);
            });
            return LoaderProgress;
        }
        /// <summary>
        /// get current state of BackgroundWorker
        /// </summary>
        /// <param name="progressableObject"></param>
        /// <param name="bworker"></param>
        private void RunGetCurrentProgress(IProgressable progressableObject, BackgroundWorker bworker)
        {
            while (true)
            {
                int current = progressableObject.CurrentProgress;

                if (bworker.IsBusy && !bworker.CancellationPending)
                {
                    try
                    {
                        bworker.ReportProgress(current);
                    }
                    catch
                    {
                        break;
                    }
                }
                else
                    break;
                if (progressableObject.MaxProgress > 0 && current >= progressableObject.MaxProgress)
                {
                    break;
                }

                Thread.Sleep(100);
            }
        }

        public void Dispose()
        {
            if (bw != null)
            {
                bw.Dispose();
                bw = null;
            }

        }
    }
}
