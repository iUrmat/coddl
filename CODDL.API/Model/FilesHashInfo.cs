﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CODDL.API.Model
{

    /// <summary>
    /// Hash info
    /// </summary>
    [Serializable]
    public class FileHashInfo
    {
        /// <summary>
        /// Full path 
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Hash code
        /// </summary>
        public int Hash { get; set; }
        /// <summary>
        /// Used when generate DDLObject
        /// </summary>
        [XmlIgnore]
        public bool CheckForDel { get; set; }
        public FileHashInfo()
        {
            CheckForDel = true;
        }
    }
    /// <summary>
    /// Hash file DDL objects
    /// </summary>
    [Serializable]
    public class FileWithHash:List<FileHashInfo>
    {
        /// <summary>
        /// File Hash name
        /// </summary>
        [XmlIgnore]
        public string FileName { get; set; }
        /// <summary>
        /// true if one of the hash codes do not match
        /// </summary>
        [XmlIgnore]
        public bool IsChanged { get; set; }
        
        public FileWithHash()
        {
            IsChanged = false ;
            
        }
        /// <summary>
        ///  Add file hash info 
        /// </summary>
        /// <param name="fileinfo"></param>
        public   new void  Add(FileHashInfo fileinfo)
        {
            base.Add(fileinfo);
            IsChanged = true;
        }
    }
}
