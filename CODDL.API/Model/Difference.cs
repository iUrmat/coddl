﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace CODDL.API.Model
{
    /// <summary>
    /// Compare DDLObjects
    /// </summary>
    public class Difference:INotifyPropertyChanged
    {
        public IDDLBundleInfo BundleInfo { get; set; }
        public string ObjectName { get; set; }
        public string ObjectOwner { get; set; }
        public string ObjectType { get; set; }
        public DDLObject DDLObject1 { get; set; }
        public DDLObject DDLObject2 { get; set; }
        private bool _IsChecked = false;
        // Selected Difference
        public bool IsChecked {
            get
            {
                return _IsChecked;
            }
            set
            {
                _IsChecked = value;
                NotifyPropertyChanged("IsChecked");
            }
        }

        public string DiffType
        {
            get
            {
                if (DDLObject1 == null)
                {
                    return " not found in FIRST bundle";
                }

                if (DDLObject2 == null)
                {
                    return " not found in SECOND bundle";
                }

                return " differs";
            }
        }
        public Difference(IDDLBundleInfo bundleInfo,DDLObject obj1,DDLObject obj2)
        {
            BundleInfo = bundleInfo;
            DDLObject1 = obj1;
            DDLObject2 = obj2;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string SaveToBundle1(string dir)
        {
            if (DDLObject1 == null) return "";
            string filename = Path.Combine(dir, string.Format("{0}.sql", 
                    DDLObject1.DDLName));
                SaveToDir.SaveToFile(filename, DDLObject1.DDLText);
                return filename;
        }
        public string SaveToBundle2(string dir)
        {
            if (DDLObject2 == null) return "";

            string filename = Path.Combine(dir, string.Format("{0}.sql",
                DDLObject1.DDLName));
            SaveToDir.SaveToFile(filename, DDLObject2.DDLText);
            return filename;
        }

        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
