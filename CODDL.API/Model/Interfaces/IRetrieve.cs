﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CODDL.API.Model
{
    /// <summary>
    /// Interface for  load command
    /// </summary>
   public interface IRetrieve:IProgressable
    {

        /// <summary>
        /// Load objects
        /// </summary>
        /// <param name="bundleInfo">IDDLBundleInfo</param>
        /// <returns>List of DDLObject</returns>
        List<DDLObject> Load(IDDLBundleInfo bundleInfo);
       
    }
}
