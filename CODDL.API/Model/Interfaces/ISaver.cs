﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODDL.API.Model
{
    /// <summary>
    /// Interface for  save operation
    /// </summary>
  public  interface ISaver: IProgressable
    {
        /// <summary>
        /// Save DDLbundle object 
        /// </summary>
        /// <param name="bundle">DDLBundle object</param>
        void Save(DDLBundle bundle);
        /// <summary>
        /// Save result message 
        /// </summary>
        string Message { get; set; }
    }
}
