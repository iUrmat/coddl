﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CODDL.API.Model
{
    /// <summary>
    /// Create DDLBundle class information
    /// </summary>
    public  interface IDDLBundleInfo:IDisposable
    {
        /// <summary>
        /// It contains source
        /// </summary>
        string BundleFrom { get; set; }
    }
}
