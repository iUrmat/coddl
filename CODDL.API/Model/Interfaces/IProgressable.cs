﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CODDL.API.Model 
{
    /// <summary>
    /// Intarface for Run the command
    /// </summary>
   public  interface IProgressable
    {
        /// <summary>
        /// Maximum command progress
        /// </summary>
        int MaxProgress { get; set; }
        /// <summary>
        /// Current  command progress
        /// </summary>
        int CurrentProgress { get; }

        object Run(DDLBundle bundle);
    }
}
