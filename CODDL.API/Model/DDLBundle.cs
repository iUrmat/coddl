﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CODDL.API.Model
{
    /// <summary>
    /// Object bundle
    /// </summary>
    [Serializable]
    public class DDLBundle : IDisposable
    {

        /// <summary>
        /// Bundle information 
        /// </summary>
        [NonSerialized]
        [XmlIgnore]
        public IDDLBundleInfo BundleInfo;
        /// <summary>
        /// List of DDLObjects
        /// </summary>
        public readonly List<DDLObject> DDLObjects = new List<DDLObject>();

        public DDLBundle()
        {
        }
        public DDLBundle(IDDLBundleInfo bundleInfo)
        {
            BundleInfo = bundleInfo;

        }


        #region IDisposable
        public void Dispose()
        {
            BundleInfo.Dispose();
            DDLObjects.Clear();

        }


        #endregion
         
    }
}
