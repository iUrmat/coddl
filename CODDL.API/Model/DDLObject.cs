﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODDL.API.Model
{
    /// <summary>
    /// DDL object
    /// </summary>
   public  class DDLObject : IEquatable<DDLObject>
    {
        /// <summary>
        /// DDL hash
        /// </summary>
        private int _DDLHash;
        ///<summary>
        /// DDL Owner
        /// </summary>
        public string Owner { get; set; }
        /// <summary>
        /// DDL type
        /// </summary>
        public string DDLType { get; set; }
        /// <summary>
        /// DDL name
        /// </summary>
        public string DDLName { get; set; }
        
      /// <summary>
      /// DDL text  
      /// </summary>
        public string DDLText { get;   set; }
        public DDLObject() { }
        public DDLObject(string owner, string ddltype, string ddlName,  string ddltext)
        {

            Owner = owner;
            DDLType = ddltype;
            DDLName = ddlName;

            DDLText = ddltext;
           
            _DDLHash = DDLText.GetHashCode();    
        }
        /// <summary>
        /// HashCode text
        /// </summary>
        public int DDLHash
        {
            get
            {
                if (_DDLHash == 0) { throw new Exception("Didn't calculate DDL for object yet"); }
                return _DDLHash;
            }
        }
        public override string ToString()
        {
            return string.Format("DDLName={0},DDLType={1},Owner={2},DDLHash={3}", DDLName, DDLType, Owner, DDLHash.ToString());
        }
        /// <summary>
        /// Generate file name 
        /// </summary>
        /// <returns>Owner\DDLType\DDLName.sql </returns>
        public string GenerateFileName()
        {
            return string.Format("{0}\\{1}\\{2}.sql", Owner, DDLType, DDLName);
        }
        /// <summary>
        /// Compare ddl object without hash
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool EqualsWithoutHash(DDLObject other)
        {
            return            
            other.DDLName == this.DDLName &&
            other.DDLType == this.DDLType &&
            other.Owner == this.Owner;
            
        }
        /// <summary>
        /// compare ddl object
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(DDLObject other)
        {

            return EqualsWithoutHash(other) &&
                other.DDLHash == this.DDLHash;
                
        }
    }
}
