﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace CODDL.API.Model
{
    /// <summary>
    /// Bundle type
    /// </summary>
      public enum BundleType
    {
        [Display(Name = "Database")]
        [Description("Working with database")]
        DB =0, // Database
        [Display(Name = "GIT repository")]
        [Description("Working with GIT repository")]
        GIT =1, //GIT repository 
        [Display(Name = "File directory")]
        [Description("Working with File directory")]
        Directory =2 // File directory
    }
    /// <summary>
    /// GIT setting information
    /// </summary>
    public class GitInfo
    {
        /// <summary>
        /// Git user name
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Git user password
        /// </summary>
        public string UserPass { get; set; }
        /// <summary>
        /// Git remote url path
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// Git local path 
        /// </summary>
        public string LocalPath { get; set; }
        /// <summary>
        /// Git user Email
        /// </summary>
        public string UserEmail { get; set; }
        /// <summary>
        /// Bundle server name (full git path: Url\ServerName)
        /// </summary>
        public string ServerName { get; set; }
         
    }
}
