﻿using CODDL.API.DB;

using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODDL.API.Model
{
    /// <summary>
    /// Load DDLObject from database
    /// </summary>
    public class DBRetrieve : IRetrieve, IDisposable
    {
        /// <summary>
        /// Setting for load
        /// </summary>
        public DDLBundleFromDBInfo DDLInfo { get; set; }

        /// <summary>
        /// Package for oracle commands
        /// </summary>
        PKG_API pkgAPI;



        /// <summary>
        /// Max number for progress
        /// </summary>
        public int MaxProgress
        {
            //get{ return }
            get { return pkgAPI.MaxRecNo; }
            set { }

        }
        /// <summary>
        /// Get current value for progress ( get in base)
        /// </summary>
        /// <returns></returns>
        public int CurrentProgress
        {
            get
            {
                return pkgAPI.CurrentRecNo;
            }
        }

        /// <summary>
        /// Load from base 
        /// </summary>
        /// <param name="info">IDDLBundleInfo</param>
        /// <returns></returns>
        public virtual List<DDLObject> Load(IDDLBundleInfo info)
        {
            List<DDLObject> Result = new List<DDLObject>();
            DDLInfo = info as DDLBundleFromDBInfo;
            try
            {
                Result = pkgAPI.GETDDLS_FOROBJECTLIST(DDLInfo.GetTypesWithSeperator, DDLInfo.GetOwnerWithSeperate, DDLInfo.ObjectNameFilter);
            }
            catch (Exception er)
            {
                throw er;
            }

            return Result;
        }

        public void Dispose()
        {
            pkgAPI.Connertor.Close();
        }
        /// <summary>
        /// Load Owners
        /// </summary>
        /// <param name="ownerFilter">Owner filter List<string></string></param>
        /// <returns>List<string> list of owners</returns>
        public List<string> LoadOwnerFromBase(List<string> ownerFilter)
        {
            List<string> result = new List<string>();
            var ownerFilterStr = String.Join(",", ownerFilter);
            var ownerFromFilter = pkgAPI.GETOWNERLIST_BYFLT(ownerFilterStr);
            foreach (DataRow row in ownerFromFilter.Rows)
                result.Add(row["OWNER"] as string);
            return result;
        }
        /// <summary>
        /// Run  load operation 
        /// </summary>
        /// <param name="bundle">DDLBundle object</param>
        /// <returns></returns>
        public virtual object Run(DDLBundle bundle)
        {
            return Load(bundle.BundleInfo);
        }

        /// <summary>
        /// Load DDLObject from database
        /// </summary>
        /// <param name="conn">OracleConnection </param>
        /// <param name="pkgApi"> PKG_API</param>
        public DBRetrieve(OracleConnection conn, PKG_API pkgApi=null)
        {

            pkgAPI =pkgApi ?? new PKG_API(conn);

        }
    }
}
