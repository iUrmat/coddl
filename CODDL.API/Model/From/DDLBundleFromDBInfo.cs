﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CODDL.API.Model
{
    /// <summary>
    /// Configuration for load in the database
    /// </summary>
    [Serializable]
    public class DDLBundleFromDBInfo:IDDLBundleInfo
    {
        /// <summary>
        /// Separator char for string
        /// </summary>
        public   const string FilterSeparator= ",";
        
        /// <summary>
        /// List of owners
        /// </summary>
        public List<string> Owners = new List<string>();
        /// <summary>
        /// List of object types
        /// </summary>
        public List<string> ObjectTypes = new List<string>();
        public DDLBundleFromDBInfo() { }

        private string _ObjectNameFilter = "";
        /// <summary>
        /// Enter list of filter expressions to reduce your schema list.
        ///All expression can use wildcards.
        ///For example, if you enter 2 expression
        ///%DB%
        ///SCO%
        ///your schema list will be filtered with
        ///(owner LIKE '%DB%' OR owner LIKE 'SCO%')
        ///Unless you enter at least one expression, 
        ///all schemas with at least one object
        ///will be shown at "Schema List" 
        /// </summary>
        public string ObjectNameFilter
        {
            get
            {
                return _ObjectNameFilter;

            }
            set
            {
                _ObjectNameFilter = value;
            }
        }
        /// <summary>
        /// Server name 
        /// </summary>
        public  string BundleFrom { get; set; }
        /// <summary>
        /// string of owners with FilterSeparator (default= ',')
        /// </summary>
        public string GetOwnerWithSeperate
        {
            get
            {

                return String.Join(FilterSeparator, Owners);
            }
        }
        /// <summary>
        /// string of owners with FilterSeparator (default= ',')
        /// </summary>
        public string GetTypesWithSeperator
        {
            get
            {
                return String.Join(FilterSeparator, ObjectTypes);
            }
        }
        /// <summary>
        /// clean 
        /// </summary>
        public void Dispose()
        {
            Owners.Clear();
            ObjectTypes.Clear();

        }

        
    }
}
