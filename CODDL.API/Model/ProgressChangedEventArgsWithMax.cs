﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODDL.API.Model
{
    /// <summary>
    /// Progress changed event with max progress value
    /// </summary>
    public class ProgressChangedEventArgsWithMax: ProgressChangedEventArgs
    {
        public int MaxProgress { get; set; }
        public ProgressChangedEventArgsWithMax(int progress, object userstate) : base(progress, userstate)
        {

        }
    }
}
