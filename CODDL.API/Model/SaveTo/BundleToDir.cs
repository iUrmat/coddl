﻿using CODDL.API;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CODDL.API.Model
{

    /// <summary>
    /// Save bundle to directory
    /// </summary>
    public class SaveToDir : ISaver
    {
        /// <summary>
        ///  The name of the encoding
        /// </summary>
        public string EncodingName { get; set; }
        
        /// <summary>
        ///The bundle index count
        /// </summary>
        private int _maxFileCount;
        /// <summary>
        /// Current bundle index
        /// </summary>
        private int currentFileIndex=0;
        /// <summary>
        /// flag for the creation of the main folder
        /// </summary>
        private bool IsCreateMaindir;
        /// <summary>
        /// The root folder for saving
        /// </summary>
        private string RootDir
        {
            get; set;
        }
        /// <summary>
        /// The server name for create folder
        /// </summary>
        private string ServerName
        {
            get;set;
        }
        /// <summary>
        /// Flag to select the folder structure
        /// </summary>
        public bool IsNotDirectoryToGitStruct = true;
        /// <summary>
        /// List of changed files
        /// </summary>
        public List<string> ChangedFiles { get; }
        /// <summary>
        /// Constructor save to file 
        /// </summary>
        /// <param name="rootdir">Root dirctory for save </param>
        /// <param name="Servername">Server name </param>
        /// <param name="createMainDir">is true save root dir </param>
        /// <param name="_EncodingName">the name of encoding, default value UTF-8</param>
        public SaveToDir(string rootdir,string Servername,bool createMainDir= true,string _EncodingName="UTF-8")
        {
            RootDir = rootdir;
            ServerName = Servername;
            IsCreateMaindir = createMainDir;
            ChangedFiles = new List<string>();
            
            EncodingName = _EncodingName;
        }
        /// <summary>
        /// save files Folder structure "BundleToDirectory"
        /// </summary>
        /// <param name="bundle"></param>
        /// <param name="MainDir"></param>
        private void SaveFileToDir(DDLBundle bundle,string MainDir)
        {
            //Grouping by Owner and types (group name {owner}_{type})
            var OwerTypes = from dll in bundle.DDLObjects
                            group dll by new { dllKey = dll.Owner + "_" + dll.DDLType } into OwnerAndType
                            select new
                            {
                                dllkey = OwnerAndType.Key.dllKey,
                                DDLOjects = OwnerAndType
                            };


            foreach (var ownerAndType in OwerTypes)
            {
                var dirName = Path.Combine(MainDir, ownerAndType.dllkey);


                dirName = CheckAndCreateDir(dirName);
                foreach (DDLObject dllobj in ownerAndType.DDLOjects)
                {

                    string filename = Path.Combine(dirName, string.Format("{0}.{1}", dllobj.DDLName, "sql"));
              
              
                    SaveToFile(filename, dllobj.DDLText, EncodingName);

                    currentFileIndex++;
                }
            }
        }
        /// <summary>
        /// Save bundle 
        /// </summary>
        /// <param name="bundle">DDLBundle</param>
        public void Save(DDLBundle bundle)
        {


            if (bundle.DDLObjects.Count == 0) return;
            

            currentFileIndex = 0;
            string MainDir =IsCreateMaindir?GetDirName():RootDir;

            if (IsNotDirectoryToGitStruct) {
                //if save bundle to folder
                SaveFileToDir(bundle, MainDir);
                return;
            }

            FileWithHash fileWithHashes = new FileWithHash();
            fileWithHashes.FileName = Path.Combine(MainDir, "HashFile.xml");
            if (File.Exists(fileWithHashes.FileName))
            {
                fileWithHashes = Utils.DeSerialize<FileWithHash>(fileWithHashes.FileName);
                fileWithHashes.FileName = Path.Combine(MainDir, "HashFile.xml");
                fileWithHashes.IsChanged = false;
            }

            //Generate  a file tree
            //Grouping by Owner and types (group name {owner}->{type})
            var Owners = from ddl in bundle.DDLObjects
                         group ddl by ddl.Owner into OwnerGroup
                         select new
                         {
                             Owner = OwnerGroup.Key,
                             Count = OwnerGroup.Count(),
                             Types = (from type in OwnerGroup
                                      group type by type.DDLType into typeGroup
                                      select new
                                      {
                                          DDLType = typeGroup.Key,
                                          Count = typeGroup.Count(),
                                          DDLOjects = typeGroup
                                      })

                         };
            //Create a folder and save files
            foreach (var owner in Owners)
            {
                Debug.WriteLine("Owner: "+owner.Owner + "-" + owner.Count.ToString());
                var ddlPath = Path.Combine(MainDir, owner.Owner);
                ddlPath = CheckAndCreateDir(ddlPath);
                //Types
                foreach (var tp in owner.Types)
                {
                    
                    Debug.WriteLine("Type: \t"+ tp.DDLType + "-" + tp.Count.ToString());
                     ddlPath = Path.Combine(MainDir,owner.Owner, tp.DDLType);
                    ddlPath = CheckAndCreateDir(ddlPath);

                    foreach (var obj in tp.DDLOjects)
                    {
                        Debug.WriteLine("DDLObj: \t\t" + obj.DDLName);

                        string filename = obj.GenerateFileName();
                        ddlPath= Path.Combine(MainDir,filename);
                        FileHashInfo filehash = fileWithHashes.FirstOrDefault(f => f.Path == filename);
                        //
                        if (filehash != null)
                        {
                            //if contain in files hash then continue
                            if (filehash.Hash == obj.DDLHash)
                            {
                                filehash.CheckForDel = false;
                                continue;
                                
                            }

                            // If the file was edited

                            filehash.Hash = obj.DDLHash;
                                fileWithHashes.IsChanged = true;
                                filehash.CheckForDel = false;
                               
                           
                        }
                        else
                        {
                            // new file
                            fileWithHashes.Add(new FileHashInfo()
                            {
                                Path = filename,
                                Hash = obj.DDLHash,
                                CheckForDel = false
                            });
                           

                        }

                        SaveToFile(ddlPath, obj.DDLText,EncodingName);
                        ChangedFiles.Add(ddlPath);
                        currentFileIndex++;
                    }
                }
            }
            //delete files, if not generated DDLObject
            foreach (var filefordel in fileWithHashes.Where(f => f.CheckForDel).ToList())
            {
                // delete files if checked
                fileWithHashes.IsChanged = true;
                string path =Path.Combine(MainDir, filefordel.Path);
                Debug.WriteLine("remove " + path);
                try
                {
                    fileWithHashes.Remove(filefordel);
                    ChangedFiles.Add(path);
                    File.Delete(path);
                }
                catch (Exception er)
                {
                    Debug.WriteLine(string.Format("{0} delete : {1}", path, er.Message), LogType.Error);
                }
            }
            if (fileWithHashes.IsChanged)
                {
                    fileWithHashes.Serialize<FileWithHash>(fileWithHashes.FileName);
                    ChangedFiles.Add(fileWithHashes.FileName);
            }
        }



        /// <summary>
        /// Save file to filename
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="DDLText"></param>
        /// <param name="encoding">default encoding="UTF-8"</param>
        /// <returns></returns>
        public static string SaveToFile(string filename, string DDLText,string encoding="UTF-8")
        {
            //string filename = Path.Combine(filename, GetFileName());

            File.WriteAllText( filename, DDLText, Encoding.GetEncoding(encoding));
            return filename;
        }
        /// <summary>
        /// Change is not valid characters in the file name
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string CleanFileName(string fileName)
        {
            return Regex.Replace(fileName.Trim(), "[^A-Za-z0-9_. ]+", "_");
        }
        /// <summary>
        /// Generate file name for save to dir
        /// </summary>
        /// <returns></returns>
        public string GetDirName()
        {
            string dirNew = Path.Combine(RootDir, string.Format("{0}_{1}", DateTime.Now.ToString("yyyyMMdd_HHmmss"),ServerName));
            dirNew = CheckAndCreateDir(dirNew);
            return dirNew;
        }
        /// <summary>
        /// Check and create directory if not exist
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        public  string CheckAndCreateDir(string dir)
        {
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return dir;
        }
        /// <summary>
        /// Run operation
        /// </summary>
        /// <param name="bundle"></param>
        /// <returns></returns>
        public object Run(DDLBundle bundle)
        {
            Save(bundle);
            return null;
               
        }
        /// <summary>
        ///   maximum progress
        /// </summary>
        public int MaxProgress
        {
            get
            {
                return _maxFileCount;
            }
            set
            {
                _maxFileCount = value;
            }
        }
        /// <summary>
        /// Current Progress
        /// </summary>
        public int CurrentProgress
        {
            get
            {
                return currentFileIndex;
            }
        }
        /// <summary>
        /// Result message
        /// </summary>
        public string Message
        {
            get; set;
        }
    }
}
