﻿
using CODDL.API;
using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace CODDL.API.Model.SaveTo
{
    /// <summary>
    /// Save bundle to GIT
    /// </summary>
    public class BundleToGit : ISaver
    {
        public string EncodingName { get; set; }
        public bool IsCommited { get; set; }
        private int MaxToGit = 100;
        private int CurrentToGit = 0;
        private ILog Logger;
        /// <summary>
        /// Maximum progress value
        /// </summary>
        public int MaxProgress
        {
            get
            {
                return MaxToGit;
            }

            set
            {
                MaxToGit = value;
            }
        }
        GitInfo gitInfo { get; set; }
        /// <summary>
        /// Current progress value
        /// </summary>
        public int CurrentProgress
        {
            get
            {
                return CurrentToGit;
            }
        }

        public string Message { get; set; }




        // private List<string> FilterObjectTypes = new List<string>();
        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="info">Information about of GIT </param>
        /// <param name="logger">Logger for save log </param>
        /// <param name="_EncodingName">Encoding for save to file (default "UTF-8")</param>
        public BundleToGit(GitInfo info,ILog logger=null, string _EncodingName = "UTF-8")// string path,string gituser,string gitUserEmail,string pass)
        {
            gitInfo = info;
            //var owners=gitInfo.OwnersForFilter.Where(o => o.IndexOf('%')>0);
            //if (owners != null)
            //    FilterOwnersWithDot = owners.ToList();
            //FilterOwners =gitInfo.OwnersForFilter.Where(o => o.IndexOf('%') <0).ToList();
            EncodingName = _EncodingName;
            Logger = logger;
            IsCommited = false;
            //LocalPath = Repository.Discover(path);
            //     path= Repository.Init(LocalPath);
            // Clone(string.Format("{0}/{1}.git", gitInfo.Url,gitInfo.ServerName),gitInfo.UserName,gitInfo.UserPass);
            Debug.WriteLine(gitInfo.LocalPath);
        }
        /// <summary>
        /// Log to ILog
        /// </summary>
        /// <param name="text"></param>
        /// <param name="type"></param>
        private void Log(string text,LogType type )
        {
            if (Logger != null)
            {
                Logger.Log(text, type);
            }
        }

        /// <summary>
        /// Save patch to directory
        /// </summary>
        /// <param name="commitTree"></param>
        /// <param name="patch"></param>
        /// <param name="outdir"></param>
        public static void SavePatchTo(Tree commitTree, Patch patch, string outdir)
        {
            foreach (var ptc in patch)
            {
                Debug.WriteLine(ptc.Status + " -> " + ptc.Path); // Status -> File Path
                var entry = commitTree[ptc.Path];
                if (entry == null) continue;
                Debug.WriteLine(entry.TargetType == TreeEntryTargetType.Blob);
                var blob = (Blob)entry.Target;
                var content = blob.GetContentStream();
                Debug.WriteLine("Size: " + blob.Size.ToString() + " " + content.Length.ToString());
                using (var tr = new StreamReader(content, Encoding.GetEncoding(1251)))
                {
                    string str = tr.ReadToEnd();
                    var localFile = Path.Combine(outdir, Path.GetFileName(ptc.Path));
                    using (var writer = new StreamWriter(localFile, false, Encoding.GetEncoding(1251)))
                    {
                        writer.Write(str);
                        Debug.WriteLine(ptc.Status + " -> " + ptc.Path + "->" + localFile);
                    }
                }
            }
        }
     
        /// <summary>
        /// Commit to GIT DDLbungle
        /// </summary>
        /// <param name="bundle"></param>
        /// <param name="ChangedFiles">List of changed files</param>
        /// <returns></returns>
        public string Commit(List<string> ChangedFiles)
        {
            string result = Message = "";
            ;
            using (var Repo = new Repository(gitInfo.LocalPath))
            {
             
             
                //if not changed files return
                if (ChangedFiles.Count == 0)
                {

                    Repo.Dispose();
                    return "Not changed files to commit";
                }
                Log(string.Format("Run stage: changed files count {0}", ChangedFiles.Count), LogType.Info) ;
                // Stage changed files
                Repo.Stage(ChangedFiles);

                // Setup the commit author
                Signature author = new Signature(gitInfo.UserName, gitInfo.UserEmail, DateTime.UtcNow);
                Log(string.Format( "Git  user: {0}, {1}",gitInfo.UserName,gitInfo.UserEmail),LogType.Info);

                // True if the index or the working directory has been altered since the last commit. False otherwise.
                if (!Repo.RetrieveStatus().IsDirty)
                    return result;
                try
                {

                    if (Repo.Head.Tip==null)
                        //if the first commit then add all the files
                       Repo.Index.All(i =>
                        {
                            result += i.Path+" (Added) \n";
                            return true;
                        });
                    //commit
                    Commit currCommit = Repo.Commit(result, author);
                }

                catch (EmptyCommitException)
                {
                    result = "Nothing changed.Skipping commit.";
                    Debug.WriteLine(result);

                    Repo.Dispose();
                    return result;
                }
                catch (Exception e)
                {
                    result = string.Format("Error while committing: {0}", e.Message);
                    Debug.WriteLine(result);
                    Repo.Dispose();
                    return result;
                }



                // Write our commit message
                StringBuilder sb = new StringBuilder();
                //get last tree
                Tree commitTree = Repo.Head.Tip.Tree;
                //Difference last and parent commit
                TreeChanges changes = null;
                if (Repo.Head.Tip.Parents.Count() > 0)
                {
                    //get preview tree
                    Tree parentCommitTree = Repo.Head.Tip.Parents.Single().Tree;
                    // compare with last and parent tree
                    changes = Repo.Diff.Compare<TreeChanges>(parentCommitTree, commitTree);
                }

                if (changes != null && changes.Count() > 0)
                {
                    string pluralFile = "file";
                    string pluralInsertion = "insertion";
                    string pluralDeletion = "deletion";
                    if (changes.Count() != 1) pluralFile = "files";
                    if (changes.Added.Count() != 1) pluralInsertion = "insertions";
                    if (changes.Deleted.Count() != 1) pluralDeletion = "deletions";

                    sb.AppendLine(string.Format("{0} {1} changed, {2} {3}(+), {4} {5}(-)",
                        changes.Count(), pluralFile, changes.Added.Count(), pluralInsertion, changes.Deleted.Count(), pluralDeletion));

                    CommitOptions commitOptions = new CommitOptions()
                    {
                        AmendPreviousCommit = true,
                        AllowEmptyCommit = false,
                        PrettifyMessage = true,
                        CommentaryChar = '#'
                    };

                    // Try to commit. If it throws, we log it.
                    try
                    {
                        // the formation of a change comment
                        foreach (TreeEntryChanges ch in changes)
                        {
                            sb.AppendLine(
                            string.Format(CultureInfo.InvariantCulture,
                                    "Path = {0}, File {1}",
                                    !string.IsNullOrEmpty(ch.Path)
                                        ? ch.Path
                                        : ch.OldPath,
                                    ch.Status));
                        }

                        Commit ammendedCommit = Repo.Commit(sb.ToString(), author, commitOptions);
                        Debug.WriteLine("Committed changes, id: " + ammendedCommit.Sha);
                        Repo.Refs.UpdateTarget(Repo.Refs.Head, ammendedCommit.Id);
                      
                        //Push to remote 

                        //Remote remote = Repo.Network.Remotes["origin"];
                        //var options = new PushOptions();
                        //options.CredentialsProvider = (_url, _user, _cred) =>
                        //    new UsernamePasswordCredentials { Username =gitInfo.UserName, Password = gitInfo.UserPass };
                        //Repo.Network.Push(remote ,Repo.Branches[], options);



                        IsCommited = true;
                        result = sb.ToString();
                    }
                    catch (EmptyCommitException)
                    {
                        result = "Nothing changed.Skipping commit.";
                        Debug.WriteLine(result);
                    }
                    catch (Exception e)
                    {
                        result = string.Format("Error while committing: {0}", e.Message);
                        Debug.WriteLine(result);
                    }
                }


            }

            return result;

        }
        /// <summary>
        /// Push to Remote GIT Server
        /// </summary>
        /// <param name="repo"></param>
        public void PushChanges(Repository repo)
        {
            try
            {
                var remote = repo.Network.Remotes["origin"];
                var options = new PushOptions();
                options.CredentialsProvider = (_url, _user, _cred) =>
                    new UsernamePasswordCredentials { Username = gitInfo.UserName, Password = gitInfo.UserPass };
                var pushRefSpec = @"refs/heads/{0}";
                foreach (var branche in repo.Branches)

                    repo.Network.Push(remote, branche.UpstreamBranchCanonicalName, options, new Signature(gitInfo.UserName, gitInfo.UserEmail, DateTimeOffset.Now),
                        "pushed changes");

                //      repo.Network.Push(repo.Branches, options);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception:RepoActions:PushChanges " + e.Message);
            }
        }
        /// <summary>
        /// Save Bundle to local Git Path
        /// </summary>
        /// <param name="bundle"></param>
        public virtual void Save(DDLBundle bundle)
        {
            //save bundle to repository directory
            SaveToDir saver = new SaveToDir(gitInfo.LocalPath, bundle.BundleInfo.BundleFrom, false);
            CurrentToGit = 10;
            saver.EncodingName = EncodingName;
            saver.IsNotDirectoryToGitStruct = false;
            //operation save to directory
            DDLOperation OperationSave = new DDLOperation(bundle);
            OperationSave.Start(saver, true);
            //bundle.SaveTo(saver, true);
            CurrentToGit = 20;
            Message = Commit(saver.ChangedFiles);
            
            CurrentToGit = 100;
        }
        // Run BundleToGit command        
        public virtual object Run(DDLBundle bundle)
        {
            Save(bundle);
            return null;
        }
       
    }
}