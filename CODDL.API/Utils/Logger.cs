﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CODDL.API
{
    /// <summary>
    /// Log type
    /// </summary>
    public enum LogType
    {
        Info,
        Warning,
        Error
    }
   public interface ILog
    {
         void Log(string message, LogType type);
    }
    public abstract class LogBase:ILog

    {
        /// <summary>
        /// Object for lock
        /// </summary>
        protected readonly object lockObj = new object();
        /// <summary>
        /// save log message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public abstract void Log( string message, LogType type);

    }
    /// <summary>
    /// class save log to file
    /// </summary>
    public class FileLogger : LogBase

    {
        /// <summary>
        /// the default path of the folder where the executable file
        /// </summary>
        public string DefaultLogFileName { get; set; }

        public string filePath = @"c:\Log.txt";
        /// <summary>
        /// show log on Console windows 
        /// </summary>
        public bool IsShowMessageInConsole { get; set; }
        /// <summary>
        /// Save log
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public override void Log(string message, LogType type=LogType.Info)

        {

            lock (lockObj)

            {

                using (StreamWriter streamWriter = new StreamWriter(filePath,true))

                {
                    string mess = string.Format("[{1} {0}] {2}",Enum.GetName(typeof(LogType), type), DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"), message);
                    if (IsShowMessageInConsole)
                        Console.WriteLine(mess);
                    streamWriter.WriteLine(mess);

                    streamWriter.Close();

                }

            }

        }

    }
}
