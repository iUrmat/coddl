﻿using CODDL.API.Model;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace CODDL.API
{
    /// <summary>
    /// Compare bundle
    /// </summary>
   public  class BundleCompare
    {
        bool IgnoreWhiteSpace = false;
        bool IgnoreCase=false;
        public BundleCompare (bool ignoreWhiteSpace,bool ignoreCase)
        {
            IgnoreWhiteSpace = ignoreWhiteSpace;
            IgnoreCase = ignoreCase;
        }
        /// <summary>
        /// Compare bundles
        /// </summary>
        /// <param name="bundle1"></param>
        /// <param name="bundle2"></param>
        /// <returns> List of Difference</returns>
        public ObservableCollection<Difference> Compare(DDLBundle bundle1,DDLBundle bundle2)
        {
            ObservableCollection<Difference> Differences = new ObservableCollection<Difference>();
            if (bundle1 == null || bundle2 == null) {
                return Differences;
                    }
            var ddlobjects1 = bundle1.DDLObjects.ToList();
            var ddlobjects2 = bundle2.DDLObjects.ToList();
            foreach (DDLObject obj in ddlobjects1)
            {
                DDLObject obj2 = bundle2==null? null : ddlobjects2.Find(b => b.EqualsWithoutHash(obj));
                if (obj2 == null)// not found, so differ is EXISTS in first, BUT NOT PRESENT in second bundle
                {
                    Difference diff = new Difference(bundle1.BundleInfo, obj, null);
                    diff.ObjectName = obj.DDLName;
                    diff.ObjectOwner = obj.Owner;
                    diff.ObjectType = obj.DDLType;
                    Differences.Add(diff);
                    continue;
                }
                // object exists in both bundle
                if (!obj.Equals(obj2))
                {
                    StringBuilder ObjectText1 =new StringBuilder();
                    StringBuilder ObjectText2 =new StringBuilder();
                    if (IgnoreCase)
                    {
                        ObjectText1.Append(obj.DDLText.ToUpper());
                        ObjectText2.Append(obj2.DDLText.ToUpper());
                    }else
                    {
                        ObjectText1.Append(obj.DDLText);
                        ObjectText2.Append(obj2.DDLText);
                    }
                    if (IgnoreWhiteSpace)
                    {
                        ObjectText1 = StringRepetitiveWhiteSpaceRemover.Filter(ObjectText1.ToString());
                        ObjectText2 = StringRepetitiveWhiteSpaceRemover.Filter(ObjectText2.ToString());
                    }
                    if (!ObjectText1.ToString().Equals(ObjectText2.ToString()))
                    {
                        Difference diff = new Difference(bundle1.BundleInfo, obj, obj2);
                        diff.ObjectName = obj.DDLName;
                        diff.ObjectOwner = obj.Owner;
                        diff.ObjectType = obj.DDLType;
                        Differences.Add(diff);
                    }
                }
                ddlobjects2.Remove(obj2);
            }
            foreach (DDLObject obj in ddlobjects2)
            {
                DDLObject obj1 = bundle1.DDLObjects.Find(b => b.EqualsWithoutHash(obj));
                if (obj1 == null)// not found, so differ is EXISTS in first, BUT NOT PRESENT in second bundle
                {
                    Difference diff = new Difference(bundle2.BundleInfo,  null,obj);
                    diff.ObjectName = obj.DDLName;
                    diff.ObjectOwner = obj.Owner;
                    diff.ObjectType = obj.DDLType;
                    Differences.Add(diff);
                }
            }
                return Differences;
        }
    }
}
