﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


using System.Xml.Serialization;


namespace CODDL.API
{
    public static class Utils
    {
        #region Serialize
        /// <summary>
        /// Serialize object to xml
        /// </summary>
        /// <typeparam name="T">type of object</typeparam>
        /// <param name="obj">object to serialize</param>
        /// <param name="filename">the file name with path</param>
        public static void Serialize<T>(this object obj,string filename)
        {
            using (Stream stream = File.Create(filename))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stream, obj);
            }
        }
        /// <summary>
        /// Deserialize xml to object<T>
        /// </summary>
        /// <typeparam name="T">type of object</typeparam>
        /// <param name="filename">the file name with path
        /// </param>
        /// <returns></returns>
        public static T DeSerialize<T>(string filename)
        {
            T result = default(T);
            using (Stream stream = File.OpenRead(filename))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                result = (T)serializer.Deserialize(stream);
            }
            return result;
        }
        #endregion
        //public static void DoEvents()
        //{
        //    Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
        //                                          new Action(delegate { }));
        //}
        //public static void Message(string mess, LogType type)
        //{
        //    switch (type) {
        //        case LogType.Info:  MessageBox.Show(mess, "Info", MessageBoxButton.OK, MessageBoxImage.Information);break;
        //        case LogType.Error: MessageBox.Show(mess, "Error", MessageBoxButton.OK, MessageBoxImage.Error); break;
        //        case LogType.Warning: MessageBox.Show(mess, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning); break;
        //        default:
        //            MessageBox.Show(mess, "Info", MessageBoxButton.OK, MessageBoxImage.Information); break;
        //    }
        //}
    }
   
}
