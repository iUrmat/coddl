﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CODDL.API
{
    /// <summary>
    /// Replaces all repetitive spaces in a string by a single space character
    /// </summary>
    public class StringRepetitiveWhiteSpaceRemover
    {
        enum AutomatStates : int{Whitespace, Text};

        public static StringBuilder Filter(string input)
        {
            AutomatStates state = AutomatStates.Text;
            StringBuilder sb = new StringBuilder(input.Length);

            for (int i = 0; i < input.Length; i++)
            {
                char tmp = input[i];
                if (Char.IsWhiteSpace(tmp) || Char.IsControl(tmp))
                {
                    switch (state)
                    {
                        case AutomatStates.Text: 
                                                sb.Append(' '); 
                                                 state = AutomatStates.Whitespace; 
                                                 break;
                    }
                }
                else
                {
                    sb.Append(tmp); 
                    state = AutomatStates.Text;
                }
            }
            return sb;
        }


    }
}
