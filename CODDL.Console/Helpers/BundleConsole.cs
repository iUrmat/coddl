﻿
using CODDL.API;
using CODDL.API.DB;
using CODDL.API.Model;
using CODDL.API.Model.SaveTo;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CODDL.CONSOLE.Helpers
{
    /// <summary>
    /// Class 
    /// </summary>
    public class BundleConsole
    {
        /// <summary>
        /// Setting run command
        /// </summary>
        ConsoleCommand fromFile;
        DDLBundle ddlBundle;
       /// <summary>
       /// Logger
       /// </summary>
        FileLogger Logger;
        public BundleConsole()
        {

            Logger = new FileLogger();
            Logger.IsShowMessageInConsole = true;
            
        }
        /// <summary>
        /// Log file name (exename.log)
        /// </summary>
        public string LogFileDefalult
        {
            get
            {
                return Logger.DefaultLogFileName;
            }
            set
            {
               
                Logger.DefaultLogFileName = value;
                Logger.filePath = Logger.DefaultLogFileName;
            }
        }

        public void Log(string message,LogType logtype=LogType.Info)
        {
            Logger.Log(message, logtype);
        }
        /// <summary>
        /// Save to log exception with inner exception
        /// </summary>
        /// <param name="mess"></param>
        /// <param name="er"></param>
        public void Log(string mess, Exception er )
        {
            Exception error = er;
            StringBuilder erMessage = new StringBuilder();
            while (error != null)
            {
                erMessage.AppendLine(er.Message);
                error = error.InnerException;
            }
            Log(mess+"\n"+erMessage.ToString(), LogType.Error);
        }
        /// <summary>
        /// the Read from xml file Commands
        /// </summary>
        /// <param name="fileXML">file name</param>
        /// <returns>if all successfully return 0</returns>
        public int LoadXML(string fileXML)
        {
             fromFile =Utils.DeSerialize<ConsoleCommand>(fileXML);

            if (fromFile == null)
                return -1;
            return 0; //if all successfully

        }
        /// <summary>
        /// Save command setting to file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public int SaveXML(string filename)
        {
            int result = 0;
            try {
                fromFile.Serialize<ConsoleCommand>(filename);
            }catch (Exception er)
            {
                result = -1;
            }
            return result;
        }
        /// <summary>
        /// Run console command
        /// </summary>
        /// <param name="xmlConfigFile">xml command file name</param>
        public void Run(string xmlConfigFile)
        {
            int result = 0;
            if (!File.Exists(xmlConfigFile))
            {

                // Временно для теста 
                 //fromFile= ForTest();
                //result = SaveXML(xmlConfigFile);
                Log("XML file does not exist!");
                return;
            }
            else
            {
                //load command
                result=LoadXML(xmlConfigFile);
                Log("Load from xml file :" + xmlConfigFile);
            }
            foreach (BundleCommand command in fromFile.Commands)
            {
                //Definition  log file name
                if (!string.IsNullOrEmpty(command.LogFileName))
                    // log file name get in command
                    if (string.IsNullOrEmpty(Path.GetDirectoryName(command.LogFileName)))
                    {
                        //if command not contain full path then save to current dir 
                        Logger.filePath = Path.Combine(Environment.CurrentDirectory, command.LogFileName);
                    }
                    else
                        Logger.filePath = command.LogFileName;
                else
                    //log file name is DefaultLogFileName
                    Logger.filePath = Logger.DefaultLogFileName;

                Log("\n".PadRight(50,'-'));
                Log(command.Retrieve.BundleFrom + ": Start");
                //Load DDLBudles (return 0 -OK)
                result = Load(command);
                if (result == 0)
                {
                    try
                    {
                        //save DDLBundle (return 0 -OK)
                        result = Save(command);
                    }
                    catch (Exception er)
                    {
                        Log(er.Message,er);
                        continue;
                    }
                    
                }
                Log(command.Retrieve.BundleFrom + ": Finish...");
            }
        }
        /// <summary>
        /// Load from 
        /// </summary>
        /// <returns></returns>
        private  int Load(BundleCommand command)
        {
            int result = 0;
            switch(command.Retrieve.RetrieveType)
            {
                case BundleType.DB:

                    //Task[] tasks = new Task[2];
                    //tasks[0]=Task.Factory.StartNew(() =>
                    //{
                        ddlBundle = LoadFromDB(command);
                    if (ddlBundle == null)
                        result = -1;
                    //});

                    
                    //Task.WaitAll(tasks);
                    break;
                default:
                    result = -1;
                    break;
            }
            
            return result;
        }
        private DDLBundle LoadFromDB(BundleCommand command)
        {
            if (command.Retrieve.RetrieveType != BundleType.DB) return null;
            DDLBundleFromDBInfo DDLBuncleInfo = new DDLBundleFromDBInfo();
            DDLBuncleInfo.BundleFrom = command.Retrieve.BundleFrom;
            DDLBuncleInfo.ObjectTypes = command.Retrieve.ObjectTypes;
            DDLBuncleInfo.ObjectNameFilter = command.Retrieve.ObjectNameFilter;

            
            DDLBundle DDLBundle1 = new DDLBundle(DDLBuncleInfo);
            try {
                OracleConnection  conn = OraConnector.Instance.connect(command.Retrieve.Login, command.Retrieve.Password, command.Retrieve.BundleFrom);
                
               // var ownerList= fromFile.ObjectOwners.Split(',').ToList<string>();
              
                


                DBRetrieve dbRetrieve = new DBRetrieve(conn);
                
                DDLBuncleInfo.Owners = dbRetrieve.LoadOwnerFromBase(command.Retrieve.ObjectOwners);
                using (DDLOperation OperationLoad = new DDLOperation(DDLBundle1))
                {
                    OperationLoad.Start(dbRetrieve, true);
                }
                //DDLBundle1.Loader(dbRetrieve,true);
                Log(string.Format("{0}: load from DB {1} ddl objects", DDLBuncleInfo.BundleFrom,DDLBundle1.DDLObjects.Count().ToString()));
            }
            catch(Exception er)
            {
                Debug.WriteLine(er.Message);
                Log(string.Format("{0}: {1}", DDLBuncleInfo.BundleFrom, er.Message),LogType.Error);
                DDLBundle1 = null;
                
            }


            return DDLBundle1;
        }
        private  int Save(BundleCommand command)
        {
            int result = 0;
            switch (command.Save.SaveType)
            {
                case BundleType.Directory:
                   System.Console.WriteLine("Save to directory");

                    result= SaveTodir(ddlBundle,command);
                        Log(command.Retrieve.BundleFrom + ": saved to directory");
                    break;
                case BundleType.GIT:
                    System.Console.WriteLine("Commit to GIT");
                    result= SaveToGit(ddlBundle,command);
                    Log(command.Retrieve.BundleFrom + ": saved to GIT");

                    break;
                default:
                    result = -1;
                    break;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bundle"></param>
        /// <returns>if all successfully retun 0</returns>
        private  int SaveTodir(DDLBundle bundle, BundleCommand command)
        {

            SaveToDir saver = new SaveToDir(command.Save.OutputPath, bundle.BundleInfo.BundleFrom);
            saver.EncodingName = command.Save.Encoding;

            using (DDLOperation OperationSave = new DDLOperation(ddlBundle))
            {
                OperationSave.Start(saver, true);
            }
            //bundle.SaveTo(saver,true);
            return 0;
        }
        /// <summary>
        /// Save bundle to GIT repository
        /// </summary>
        /// <param name="bundle"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        private int SaveToGit(DDLBundle bundle, BundleCommand command)
        {

            GitInfo info = new GitInfo();
            try {
                info.LocalPath = System.IO.Path.Combine(command.Save.OutputPath, bundle.BundleInfo.BundleFrom);
                Log(string.Format("{0}: GIT repository path ({1})", bundle.BundleInfo.BundleFrom, info.LocalPath));
                //   info.Url = Properties.Settings.Default.GitUrl;
                info.UserName = command.Save.GitUserName;
                info.UserEmail = command.Save.GitUserEmail;
                info.UserPass = command.Save.GitUserPass;
                info.ServerName = bundle.BundleInfo.BundleFrom;
             
                BundleToGit saver = new BundleToGit(info, Logger);
                saver.EncodingName = command.Save.Encoding;
                using (DDLOperation OperationSave = new DDLOperation(ddlBundle))
                {
                    OperationSave.Start(saver, true);
                }
                //bundle.SaveTo(saver, true);
                Log(string.Format("{0}: save to git ({1})", bundle.BundleInfo.BundleFrom, saver.Message));
            }catch (Exception er)
            {
                Log(string.Format("{0}: ", bundle.BundleInfo.BundleFrom, er.Message),er);
                
                return -1;
            }
            return 0;
        }
        /// <summary>
        /// Generate default Command xml file
        /// </summary>
        /// <returns></returns>
        public ConsoleCommand ForTest()
        {
            BundleCommand result = new BundleCommand();
            result.Retrieve.RetrieveType = BundleType.DB;
            result.Save.SaveType = BundleType.GIT;
            result.Retrieve.BundleFrom = "KRDEV12";
            result.Retrieve.Login = "CODDL";
            result.Retrieve.Password = "CODDLPASS";
            result.Retrieve.ObjectNameFilter = "";
            result.Retrieve.ObjectTypes.AddRange(new List<string>() { "PACKAGE", "PACKAGE BODY" });
            result.Retrieve.ObjectOwners.AddRange(new List<string>() { "DB%", "KR%" });
            result.Save.OutputPath = @"C:\GIT\Repositories";

            BundleCommand result2 = result.Clone() as BundleCommand;
            result2.Retrieve.BundleFrom = "KRHOT";
            ConsoleCommand command = new ConsoleCommand();
            command.Commands.Add(result);
            command.Commands.Add(result2);
            return command;

        }
    }
}
