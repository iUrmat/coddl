﻿using CODDL.API;

using CODDL.CONSOLE.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODDL.CONSOLE
{
    class Program
    {
        static void Main(string[] args)
        {
             Console.WriteLine("Start");

            BundleConsole bundleConsole = new BundleConsole();

            string CurrentPath = Path.Combine(Environment.CurrentDirectory, System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".log");
            bundleConsole.LogFileDefalult = CurrentPath;

            if (args.Count() < 2 || !args[0].ToUpper().StartsWith("-CONFIG"))
            {
                Console.WriteLine("Console version command format:");
                Console.WriteLine(" -CONFIG CommandXmlFile.xml");
                Console.WriteLine("");

                return;
            }
            bundleConsole.Log("\n".PadRight(50, '-'));
            bundleConsole.Log("Start bundle console ");
            string CommandFileName = args[1];
            if (string.IsNullOrEmpty(Path.GetDirectoryName(CommandFileName)))
                CommandFileName = Path.Combine(Environment.CurrentDirectory, CommandFileName);
            try
            {
                bundleConsole.Run(CommandFileName);
                //restore log file name 
                bundleConsole.LogFileDefalult = CurrentPath;
                bundleConsole.Log("Finish");
            }
            catch (Exception er)
            {
                //restore log file name 
                bundleConsole.LogFileDefalult = CurrentPath;
                bundleConsole.Log(er.Message, LogType.Error);
                if (er.InnerException != null)
                    bundleConsole.Log("Inner error: " + er.InnerException.Message);

            }
        }
    }
}
