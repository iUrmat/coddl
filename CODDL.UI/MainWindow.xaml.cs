﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace CODDL.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void OnMenuClose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OnSettings(object sender, RoutedEventArgs e)
        {
            Windows.Settings settting = new Windows.Settings();
            settting.Owner = this;
            settting.ShowDialog();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            ucLoadFromDB.SaveSettings();
            viewGit.SaveSettings();
            base.OnClosing(e);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            ucLoadFromDB.SaveSettingToXML();
        }
    }
}
