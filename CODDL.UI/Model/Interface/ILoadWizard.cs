﻿using CODDL.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CODDL.UI.Model
{
    interface ILoadWizard
    {
        
        DDLBundle Load();
    }
}
