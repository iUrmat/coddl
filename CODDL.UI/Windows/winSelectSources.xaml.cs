﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CODDL.UI.Helpers;
using CODDL.API.Model;

namespace CODDL.UI.Windows
{

    /// <summary>
    /// Interaction logic for winSelectSources.xaml
    /// </summary>
    public partial class winSelectSources : Window
    {
        public winSelectSources()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            BundleType source1 =(BundleType)cbxType1.SelectedItem;
            BundleType source2 = (BundleType)cbxType2.SelectedItem;
            winLoad LoadWindow = new winLoad(source1, source2,this);
            this.Visibility = Visibility.Hidden;
            LoadWindow.ShowDialog();
            
                if (LoadWindow.IsClickCancel)
                this.Visibility = Visibility.Visible;
            
        }
    }
}
