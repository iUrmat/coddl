﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CODDL.UI.UserControls;
using CODDL.API.Model;

namespace CODDL.UI.Windows
{
    /// <summary>
    /// Interaction logic for winCompare.xaml
    /// </summary>
    public partial class winCompare : Window
    {
        
        CompareBundles compareControl;
        public winCompare(DDLBundle bundle1, DDLBundle bundle2)
        {
            
            InitializeComponent();
            compareControl = new CompareBundles(bundle1, bundle2);
            MainGrid.Children.Add(compareControl);
            compareControl.Height = double.NaN;
            compareControl.Width = double.NaN;
        }
    }
}
