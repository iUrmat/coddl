﻿using CODDL.API.DB;
using CODDL.API.Model;
using CODDL.UI.Helpers;
using CODDL.UI.Model;
using CODDL.UI.UserControls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CODDL.UI.Windows
{
    enum SelectPanel
    {
        Left,
        Right
    }
    /// <summary>
    /// Interaction logic for winLoad.xaml
    /// </summary>
    public partial class winLoad : Window
    {
        BundleType Type1;
        BundleType Type2;

        bool IsLeftCompleted, IsRightCompleted;

        Dictionary<string, DDLBundle> DDLBandles = new Dictionary<string, DDLBundle>();
        Window PreviewWindow;
        public bool IsClickCancel { get; set; }
        public winLoad(BundleType type1, BundleType type2,Window preview )
        {
            Type1 = type1;
            Type2 = type2;
            PreviewWindow = preview;
            InitializeComponent();

            Init();


        }
        private void Init()
        {
            var panel1 = CreatePanel(Type1, SelectPanel.Left);
            if (panel1 != null)
            {
                MainGrid.Children.Add(panel1);
                Grid.SetColumn(panel1, 0);
                Grid.SetRow(panel1, 0);
            }
            var panel2 = CreatePanel(Type2, SelectPanel.Right);
            if (panel2 != null)
            {
                MainGrid.Children.Add(panel2);
                (panel2 as Control).HorizontalAlignment = HorizontalAlignment.Stretch;
                Grid.SetColumn(panel2, 1);
                Grid.SetRow(panel2, 0);
            }
            //Create filter panel for load from db
            btnNext.Click += BtnNext_Click;
            btnCancel.Click += BtnCancel_Click;
          

        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            IsClickCancel = true;
            PreviewWindow.Visibility=Visibility.Visible;
               Close();
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            btnNext.IsEnabled = false;
            IsLeftCompleted =IsRightCompleted= false;
            foreach (var ctrl in MainGrid.Children)
                if (ctrl is ILoadWizard)
                    (ctrl as ILoadWizard).Load();
            //DialogResult = true;
            while (true)
            {
                if (btnNext.IsEnabled) break;
                Thread.Sleep(100);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                                                  new Action(delegate { }));
            }
            
            this.Visibility = Visibility.Hidden;
            winCompare windowCompare = new winCompare(DDLBandles.First().Value, DDLBandles.Last().Value);
            windowCompare.ShowDialog();
            IsClickCancel = false;
            this.Visibility = Visibility.Visible;
            this.Activate();
            
        }

        private ucFilterPanel AddFilterPanelForDB(LoginControlExpander MainLogin)
        {

            foreach (var ctrl in MainGrid.Children)
            {
                if (ctrl is ucFilterPanel)
                {
                    return ctrl as ucFilterPanel;
                }
            }
            ucFilterPanel filterpanel = CreateFilterPanel(MainLogin);
            
            
            MainGrid.Children.Add(filterpanel);

            if (Type1 == BundleType.DB && Type2 == BundleType.DB)
            {
                
                Grid.SetColumn(filterpanel, 0);
                Grid.SetColumnSpan(filterpanel, 2);
                Grid.SetRow(filterpanel, 2);
                
            }
            else if (Type1 == BundleType.DB)
            {
            
                Grid.SetColumn(filterpanel, 0);
                Grid.SetRow(filterpanel, 2);
            }
            else if (Type2 == BundleType.DB)
            {
            
                Grid.SetColumn(filterpanel, 1);
                Grid.SetRow(filterpanel, 2);
            }
            return filterpanel;
        }
        private ucFilterPanel CreateFilterPanel(LoginControlExpander mainLogin) {
            ucFilterPanel filterpanel = new ucFilterPanel(mainLogin);
            filterpanel.Width = double.NaN;
            filterpanel.Height = double.NaN;
                return filterpanel;
        }
        private  void LoadDbSettings(LoginControlExpander logincontrol, ucFilterPanel fltPanel, SelectPanel panel)
        {
            if (panel == SelectPanel.Left)
            {
                logincontrol.ServerName = Properties.Settings.Default.ServerName1;
                logincontrol.Login = Properties.Settings.Default.Server1Login;
                logincontrol.psPass.Password = PasswordCoder.DecryptText(Properties.Settings.Default.ServerPass1);
                logincontrol.AutoLogin = Properties.Settings.Default.AutoLogin;
              
            }
            else
            {
                logincontrol.AutoLogin = Properties.Settings.Default.AutoLoginSecondServer;

                logincontrol.ServerName = Properties.Settings.Default.ServerName2;
                logincontrol.Login = Properties.Settings.Default.Server2Login;
                logincontrol.psPass.Password = PasswordCoder.DecryptText(Properties.Settings.Default.ServerPass2);
            }
            fltPanel.txtOwnerFlt.Text = Properties.Settings.Default.OwnerFilter;
        }
        private void SaveDbSettings(LoginControlExpander loginpanel , ucFilterPanel fltPanel,SelectPanel panel)
        {
            if (panel == SelectPanel.Left)
            {

                Properties.Settings.Default.ServerName1 = loginpanel.ServerName;
                Properties.Settings.Default.Server1Login = loginpanel.Login;
                Properties.Settings.Default.AutoLogin = loginpanel.AutoLogin;

                
                Properties.Settings.Default.ServerPass1 = PasswordCoder.EncryptText(loginpanel.GetPassword());
                

                
                
                
            }
            else
            {

                Properties.Settings.Default.ServerName2 = loginpanel.ServerName;
                Properties.Settings.Default.Server2Login = loginpanel.Login;
                Properties.Settings.Default.ServerPass2 = PasswordCoder.EncryptText(loginpanel.GetPassword());
                Properties.Settings.Default.AutoLoginSecondServer = loginpanel.AutoLogin;

            }
            Properties.Settings.Default.SelectedTypes = String.Join(",", fltPanel.lstObjectTypes.GetSelectedValue);
            Properties.Settings.Default.SelectedOwners = String.Join(",", fltPanel.lstOwnerList.GetSelectedValue);
            Properties.Settings.Default.OwnerFilter = fltPanel.txtOwnerFlt.Text;

            Properties.Settings.Default.Save();
        }

        private void OnLoginDB(LoginControlExpander LoginControl, SelectPanel panel, ProgressBar progressbar, ucFilterPanel filterpanel)
        {
            if (!LoginControl.IsConnected) return;

            if (filterpanel == null) return;
            PKG_API api = new PKG_API(LoginControl.Conn);
            filterpanel.OnClickFill += (sender, arg) =>
            {
                filterpanel.lstOwnerList.ItemsSource = api.GETOWNERLIST_BYFLT(filterpanel.txtOwnerFlt.Text).DefaultView;
            };

            filterpanel.lstObjectTypes.SelectedValuePath = "OBJECTTYPE";
            filterpanel.lstObjectTypes.DisplayMemberPath = "OBJECTTYPE";

            filterpanel.lstObjectTypes.ItemsSource = api.GETOBJECTTYPELIST().DefaultView;

            filterpanel.lstOwnerList.SelectedValuePath = "OWNER";
            filterpanel.lstOwnerList.DisplayMemberPath = "OWNER";
            //Run OnClickFill
            filterpanel.OnClick();
            //Restore selected values
            if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedTypes))
            {
                var items = filterpanel.lstObjectTypes.Items.OfType<DataRowView>();
                var saveSelectedItems = Properties.Settings.Default.SelectedTypes.Split(',');
                var Finditems = items.Where(i => saveSelectedItems.Contains(i[filterpanel.lstObjectTypes.SelectedValuePath].ToString()));
                Finditems.All(f =>
                {
                    filterpanel.lstObjectTypes.SelectedItems.Add(f);
                    return true;
                });
            }
            if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedOwners))
            {
                filterpanel.lstOwnerList.SelectedItems.Clear();
                var items = filterpanel.lstOwnerList.Items.OfType<DataRowView>();
                var saveSelectedItems = Properties.Settings.Default.SelectedOwners.Split(',');
                var findItems = items.Where(i => saveSelectedItems.Contains(i[filterpanel.lstOwnerList.SelectedValuePath].ToString()));
                findItems.All(f => {
                    filterpanel.lstOwnerList.SelectedItems.Add(f); return true;
                });
            }
            if (panel == SelectPanel.Left)
                IsLeftCompleted = true;
            else
                IsRightCompleted = true;

            btnNext.IsEnabled = IsLeftCompleted && IsRightCompleted;
        }
        private void OnLoadWizardDB(LoginControlExpander LoginControl, SelectPanel panel, ProgressBar progressbar, ucFilterPanel filterpanel)
        {
            SaveDbSettings(LoginControl, filterpanel, panel);
            var selTypes = filterpanel.lstObjectTypes.GetSelectedValue;
            var selOwners = filterpanel.lstOwnerList.GetSelectedValue;
            //DDL object info
            DDLBundleFromDBInfo DDLInfoFromDB = new DDLBundleFromDBInfo();
            DDLInfoFromDB.BundleFrom = LoginControl.ServerName;
            DDLInfoFromDB.ObjectTypes.AddRange(selTypes);
            DDLInfoFromDB.ObjectNameFilter = Properties.Settings.Default.ObjectNameFilter;
            DDLInfoFromDB.Owners.AddRange(selOwners);

            //server 1

            var DDLBundle1 = new DDLBundle(DDLInfoFromDB);
            DDLOperation OperationLoad = new DDLOperation(DDLBundle1);

            OperationLoad.OnProgressChanged += (s, e) =>
            {
                progressbar.Maximum = e.MaxProgress;
                progressbar.Value = e.ProgressPercentage;
                Debug.WriteLine(progressbar.Name + ": " + e.MaxProgress.ToString() + "-" + e.ProgressPercentage.ToString());
            }; ;
            //  Bundle_OnProgressChanged;
            OperationLoad.OnLoadCompleted += (bun, arg) =>
            {
               
                DDLBandles[LoginControl.ServerName] = DDLBundle1;

                if (panel == SelectPanel.Left)
                    IsLeftCompleted = true;
                else
                    IsRightCompleted = true;

                btnNext.IsEnabled = IsLeftCompleted && IsRightCompleted;
               
            };

            // read from database
            DBRetrieve dbRetrieve = new DBRetrieve(LoginControl.Conn);

            OperationLoad.Start(dbRetrieve);
        }
        private UIElement CreatePanel(BundleType type, SelectPanel panel)
        {
            UIElement result = null;
            ProgressBar progressbar = panel == SelectPanel.Left ? prgForLeft : prgForRight;
            switch (type)
            {
                case BundleType.DB:
                    var LoginControl = new LoginControlExpander();
                    LoginControl.Width = double.NaN;
                    LoginControl.Height = double.NaN;
                    LoginControl.IsExpanded = true;
                    LoginControl.Margin = new Thickness(5);
                    
                    
                    result = LoginControl;
                    ucFilterPanel filterpanel=AddFilterPanelForDB(LoginControl);
                    LoadDbSettings(LoginControl, filterpanel, panel);
                    progressbar.Tag = LoginControl.ServerName;
                    
                    LoginControl.OnLogin += (s, e) =>
                    {
                        OnLoginDB(LoginControl, panel, progressbar,filterpanel);
                    };
                    LoginControl.OnLoadWizard += (obj, args)=>{

                        OnLoadWizardDB(LoginControl, panel, progressbar, filterpanel);
                    };
                    if (LoginControl.AutoLogin)
                        LoginControl.ClickLogin();
                    break;

            }
            return result;
        }
    }
}
