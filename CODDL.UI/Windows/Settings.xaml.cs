﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CODDL.UI.Windows
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            InitializeComponent();
            LoadSettings();
        }
        private void SaveSettings()
        {
            //List<string> list = (from l in dataGrid.ItemsSource.OfType<ObjectsNameFilter>()
            //                     select l.Filter).ToList();

            StringCollection toSave = new StringCollection();
       //     toSave.AddRange(list.ToArray());
           // Properties.Settings.Default.ObjectNameFilterList = toSave;
            Properties.Settings.Default.GITLocalPath = txtLocalGitPath.Text;
           // Properties.Settings.Default.GitUrl = txtGitURL.Text;
            Properties.Settings.Default.ObjectNameFilter = txtObjectNameFilter.Text;
            
            Properties.Settings.Default.OutputBunldeDir = txtOutputDir.Text;
            Properties.Settings.Default.Save();

        }
        protected override void OnClosing(CancelEventArgs e)
        {

            SaveSettings();
            base.OnClosing(e);
        }
        private void setDefault()
        {
            //List<ObjectsNameFilter> filters = new List<ObjectsNameFilter>();
            //filters.Add(new ObjectsNameFilter() { Filter = "%" });
            //dataGrid.ItemsSource = filters;
            SaveSettings();
        }
        private void LoadSettings()
        {
            //var ObjectNameFilterList = Properties.Settings.Default.ObjectNameFilterList.Cast<string>();
            //List<ObjectsNameFilter> filters = (from f in ObjectNameFilterList
            //              select   new ObjectsNameFilter { Filter = f }).ToList<ObjectsNameFilter>();
            //dataGrid.ItemsSource = filters;
            txtLocalGitPath.Text = Properties.Settings.Default.GITLocalPath;
       //     txtGitURL.Text = Properties.Settings.Default.GitUrl;
            txtObjectNameFilter.Text = Properties.Settings.Default.ObjectNameFilter;
            //flgOpenCompareAfterLoad.IsChecked = Properties.Settings.Default.flgOpenCompareAfterLoad;
            txtOutputDir.Text = Properties.Settings.Default.OutputBunldeDir ;
        }

        private void btnDiscard_Click(object sender, RoutedEventArgs e)
        {
            LoadSettings();
            Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void Browser_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            fbd.SelectedPath = txtOutputDir.Text;
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtOutputDir.Text = fbd.SelectedPath;
            }
        }

    }
    class ObjectsNameFilter
    {
        public string Filter { get; set; }
    }
}
