﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LibGit2Sharp;
using System.IO;
using System.Collections.ObjectModel;
using System.ComponentModel;

using System.Windows.Threading;
using CODDL.API.Model.SaveTo;

namespace CODDL.UI.UserControls
{


    public class GitPatch
    {
        public string Name { get; set; }
        public string State { get; set; }
        public string diffPatch { get; set; }
        public bool IsExpanded { get; set; }
        public Tree ComitTree { get; set; }
        public Patch Patch { get; set; }
        public  Brush StateColor {
            get
            {
                switch(State)
                {
                    case "(Deleted)":
                        return Brushes.Red;
                    case "(Added)":
                        return Brushes.Green;
                    case "(Modified)":
                        return Brushes.Blue;
                    default:
                        return Brushes.Black;
                }
            }
        }
        public ObservableCollection<GitPatch> Patches { get; set; }

        public GitPatch()
        {
            Patches = new ObservableCollection<GitPatch>();
            State = "";
            diffPatch = "";
        }
    }

    public class GitRepostiry
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public bool IsExpanded { get; set; }
        public ObservableCollection<CommitInfo> Commits { get; private set; }
        public GitRepostiry()
        {
            Commits = new ObservableCollection<CommitInfo>();
            IsExpanded = true;
        }
    }
    public class CommitInfo
    {
        public string Name { get; set; }

        public string Sha { get; set; }
        public string ShortMessage { get; set; }
        public string Message { get; set; }
        public string Path { get; set; }
        public CommitInfo()
        {

        }


    }
    /// <summary>
    /// Interaction logic for BunldeGITView.xaml
    /// </summary>
    public partial class BunldeGITView : UserControl
    {
        private string LocalGitPath { get; set; }
        Collection<GitRepostiry> CommitTree = new Collection<GitRepostiry>();
        public BunldeGITView()
        {
            InitializeComponent();

          
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
               // InitRepositry();
            }
            

        }
        public void SaveSettings()
        {
            Properties.Settings.Default.GITSavePath = txtFolder.Text;
            Properties.Settings.Default.Save();
        }
        //Fill Commit Tree view 
        public void InitRepositry()
        {
            LocalGitPath = Properties.Settings.Default.GITLocalPath;
            CommitTree.Clear();
            foreach (string dir in Directory.GetDirectories(LocalGitPath))
            {
                GitRepostiry commitRoot = new GitRepostiry() { Name = dir.Split(System.IO.Path.DirectorySeparatorChar).Last() };
                try {
                    using (var repo = new Repository(dir))
                    {
                        var commits = repo.Commits;// .OrderByDescending(o => o.Id);
                        foreach (var com in commits)
                        {
                            var name = com.MessageShort.Split(new string[] { " Path" }, StringSplitOptions.RemoveEmptyEntries);
                            CommitInfo comInfo = new CommitInfo()
                            {
                                Name = name.First(),
                                ShortMessage = string.Format("{0},{1}\n{2}", com.Author.Name, com.Author.When.ToString(), com.MessageShort),
                                Message = com.Message,
                                Sha = com.Sha,
                                Path = dir
                            };

                            commitRoot.Commits.Add(comInfo);
                        }

                        CommitTree.Add(commitRoot);
                    }
                }catch(Exception er) {
   //                 MessageBox.Show(er.Message, dir, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                // treeCommits.ItemsSource = CommitTree;
            }
            DataContext = new { CommitTree = CommitTree };
            txtFolder.Text = Properties.Settings.Default.GITSavePath;
        }

        private void treeCommits_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            CommitInfo commitinfo = e.NewValue as CommitInfo;

            if (commitinfo != null)
            {
                txtComment.Text = commitinfo.ShortMessage.Replace(" Path ", "\nPath ");
                var repo = new Repository(commitinfo.Path);



                var commit = repo.Head.Commits.FirstOrDefault(c => c.Sha == commitinfo.Sha);
                var encodecom = CpAsEncoding(commit);
                Tree commitTree = commit.Tree;
                if (commit.Parents.Count() == 0) return;
                Tree parentCommitTree = commit.Parents.Single().Tree; // Secondary Tree
                lstFiles.ItemsSource = null;
                var patchsGIT = repo.Diff.Compare<Patch>(parentCommitTree, commitTree); // Difference


                GitPatch rootPatch = new GitPatch() { Name = "PATCHES", IsExpanded = true };
                ObservableCollection<GitPatch> patchs = rootPatch.Patches; //  new ObservableCollection<GitPatch>();
                foreach (var patchGIT in patchsGIT)
                {
                    string[] dirs = patchGIT.Path.Split(System.IO.Path.DirectorySeparatorChar);
                    GitPatch gitpatch = patchs.FirstOrDefault(p => p.Name == dirs[0]);
                    if (gitpatch == null)
                    {
                        gitpatch = new GitPatch() { Name = dirs[0] };
                        patchs.Add(gitpatch);
                    }
                    GitPatch tmp = gitpatch;
                    for (int ind = 1; ind < dirs.Count(); ind++)
                    {

                        var tmp1 = tmp.Patches.FirstOrDefault(p => p.Name == dirs[ind]);
                        if (tmp1 == null)
                        {
                            tmp1 = new GitPatch() { Name = dirs[ind] };
                            tmp.Patches.Add(tmp1);
                        }

                        if (ind == dirs.Length - 1)
                        {

                            tmp1.diffPatch = patchGIT.Patch;

                            tmp1.Name = tmp1.Name;
                            tmp1.State = "(" + patchGIT.Status.ToString() + ")";
                            tmp1.Patch = patchsGIT;

                            tmp1.ComitTree = commitTree;
                        }


                        tmp = tmp1;
                    }


                    var mainPatches = new ObservableCollection<GitPatch>();
                    mainPatches.Add(rootPatch);
                    lstFiles.ItemsSource = mainPatches;

                }


                //   var patch = repo.Diff.Compare<Patch>(parentCommitTree, commitTree); // Differ
            }
        }
        private string convertTo1251(string str)
        {
            return Encoding.UTF8.GetString(Encoding.ASCII.GetBytes(str));
            /*
            Encoding srcEncodingFormat = Encoding.ASCII;
            Encoding dstEncodingFormat = Encoding.GetEncoding("windows-1251");
            byte[] originalByteString = srcEncodingFormat.GetBytes(str);
            byte[] convertedByteString = Encoding.Convert(srcEncodingFormat,
            dstEncodingFormat, originalByteString);
            string finalString = dstEncodingFormat.GetString(convertedByteString);
            return finalString;*/
        }
        public Encoding CpAsEncoding(Commit commit)
        {
            try
            {
                var encoding = commit.Encoding;

                if (encoding.StartsWith("cp", StringComparison.OrdinalIgnoreCase))
                    return Encoding.GetEncoding(int.Parse(encoding.Substring(2)));

                return Encoding.GetEncoding(encoding);
            }
            catch
            {
                return Encoding.UTF8;
            }
        }
        private void btnSeldir_Click(object sender, RoutedEventArgs e)
        {
            using (System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog())
            {
                if (!string.IsNullOrEmpty(txtFolder.Text))
                    dlg.SelectedPath = txtFolder.Text;
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    txtFolder.Text = dlg.SelectedPath;


                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var select = lstFiles.SelectedItem as GitPatch;
            if (select != null)
            {
                CommitInfo commitInfo = treeCommits.SelectedItem as CommitInfo;
                //using (var repo = new Repository(commitInfo.Path))
                //{
                if (select.ComitTree != null && select.Patch != null)
                {

                    BundleToGit.SavePatchTo(select.ComitTree, select.Patch, txtFolder.Text);
                }
                else
                {
                    ObservableCollection<GitPatch> patches = new ObservableCollection<GitPatch>();
                    if (select.Name == "PATCHES")
                    {
                        patches = select.Patches;
                    }
                    else
                        patches.Add(select);

                    foreach (GitPatch sel in patches)
                    {
                        string path = System.IO.Path.Combine(txtFolder.Text, sel.Name);
                        System.IO.Directory.CreateDirectory(path);
                        foreach (GitPatch patch in sel.Patches)
                            if (patch.ComitTree != null && patch.Patch != null)
                                BundleToGit.SavePatchTo(patch.ComitTree, patch.Patch, path);
                    }
                }
                //}
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            InitRepositry();
        }

        private void lstFiles_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            GitPatch selected = e.NewValue as GitPatch;
            if (selected != null)
            {
                //MemoryStream stream = new MemoryStream(ASCIIEncoding.Default.GetBytes(selected.diffPatch));

                //   StyledDiff(selected.diffPatch);
                viewDiff.Selection.Text = selected.diffPatch;
                StyledDiff();

            }
        }
        private void StyledDiff()
        {


            SolidColorBrush color = Brushes.Black;

            //Dispatcher.Invoke(DispatcherPriority.Normal, new DispatcherOperationCallback(delegate
            //{
            //    var newExternalParagraph = new Paragraph(new Run(str)) { Foreground =color };
            //    viewDiff.Document.Blocks.Add(newExternalParagraph);
            //    return null;
            //}), null);

            foreach (var paragraph in viewDiff.Document.Blocks)
            {
                var str = new TextRange(paragraph.ContentStart,
                               paragraph.ContentEnd).Text;
                if (str.StartsWith("@@"))
                    color = Brushes.Blue;
                else if (str.StartsWith("+"))
                    color = Brushes.Green;
                else if (str.StartsWith("-"))
                    color = Brushes.Red;
                else
                    color = Brushes.Black;
                paragraph.Foreground = color;
                paragraph.LineHeight = 1;
            }


        }
    }

}
