﻿
using CODDL.API.DB;
using CODDL.API.Model;
using CODDL.API.Model.SaveTo;
using CODDL.UI.Helpers;
using CODDL.API;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CODDL.UI.UserControls
{
    /// <summary>
    /// Interaction logic for LoadFromDB.xaml
    /// </summary>
    public partial class LoadFromDB : UserControl
    {
     //  DDLBundle DDLBundle1;
      //  DDLBundle DDLBundle2;
        Dictionary<string, DDLBundle> DDLBandles = new Dictionary<string, DDLBundle>();
        bool isCompleted1 = false;
        bool isCompleted2=false;
        bool isAutoRunCompare = false;
        public LoadFromDB()
        {
            InitializeComponent();
            MainServer.OnLogin += MainServer_OnLogin;
           // grpResult.Visibility = Visibility.Hidden;
           // grpCompareBundle.Visibility = Visibility.Hidden;
            Progress1.Visibility = Visibility.Hidden;
            Progress2.Visibility = Visibility.Hidden;
            
            LoadSettings();
            Init();     
        }

        private void Init()
        {
            if (MainServer.AutoLogin   && !string.IsNullOrEmpty(MainServer.GetPassword()))
            {
                MainServer.ClickLogin();
            }
            if (SecondServer.AutoLogin && !string.IsNullOrEmpty(SecondServer.GetPassword()))
            {
                SecondServer.ClickLogin();
            }
        }

        public void SaveSettings()
        {
            Properties.Settings.Default.ServerName1 = MainServer.ServerName;
            Properties.Settings.Default.Server1Login = MainServer.Login;
            Properties.Settings.Default.ServerName2 = SecondServer.ServerName;
             Properties.Settings.Default.Server2Login = SecondServer.Login;
            Properties.Settings.Default.AutoLogin = MainServer.AutoLogin ;
            Properties.Settings.Default.AutoLoginSecondServer = SecondServer.AutoLogin;
            
            Properties.Settings.Default.flgSameLogin = chkUserLogin.IsChecked.Value;
            Properties.Settings.Default.ServerPass1 = PasswordCoder.EncryptText(MainServer.GetPassword());
            Properties.Settings.Default.ServerPass2 = PasswordCoder.EncryptText(SecondServer.GetPassword());
            
            Properties.Settings.Default.OwnerFilter = txtOwnerFlt.Text;
            Properties.Settings.Default.SelectedTypes = String.Join(",",lstObjectTypes.GetSelectedValue);
            Properties.Settings.Default.SelectedOwners=String.Join(",", lstOwnerList.GetSelectedValue);
            Properties.Settings.Default.Save();
        }
        private void LoadSettings()
        {
            MainServer.ServerName = Properties.Settings.Default.ServerName1;
            MainServer.Login = Properties.Settings.Default.Server1Login;
            MainServer.psPass.Password =PasswordCoder.DecryptText(Properties.Settings.Default.ServerPass1);
            MainServer.AutoLogin = Properties.Settings.Default.AutoLogin;
            SecondServer.AutoLogin = Properties.Settings.Default.AutoLoginSecondServer;
            
            SecondServer.ServerName = Properties.Settings.Default.ServerName2;
            SecondServer.Login = Properties.Settings.Default.Server2Login;
            SecondServer.psPass.Password = PasswordCoder.DecryptText(Properties.Settings.Default.ServerPass2);
            
            
            
            chkUserLogin.IsChecked = Properties.Settings.Default.flgSameLogin;
            txtOwnerFlt.Text= Properties.Settings.Default.OwnerFilter;
        }
        
        private void MainServer_OnLogin(object sender, Helpers.LoginEventArgs e)
        {
            Progress1.Visibility = Visibility.Hidden;
            Progress2.Visibility = Visibility.Hidden;
            if (e.Conn != null && e.Conn.State == System.Data.ConnectionState.Open)
            {
                if (chkUserLogin.IsChecked.Value)
                {
                    SecondServer.Conn = OraConnector.Instance.connect(MainServer.Login, MainServer.psPass.Password, SecondServer.ServerName);

                }
                else
                {
                    if (SecondServer.Conn!=null)
                        SecondServer.Conn.Close();
                }
                PKG_API api = new PKG_API(MainServer.Conn);
                lstObjectTypes.SelectedValuePath = "OBJECTTYPE";
                lstObjectTypes.DisplayMemberPath = "OBJECTTYPE";

                lstObjectTypes.ItemsSource = api.GETOBJECTTYPELIST().DefaultView;

                lstOwnerList.SelectedValuePath = "OWNER";
                lstOwnerList.DisplayMemberPath = "OWNER";
                //DataTable OwnersTable = api.GETOWNERLIST();
                //load from base by filter 
                btnLoadOwner_Click(null, null);

                if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedTypes))
                {
                    var items = lstObjectTypes.Items.OfType<DataRowView>();
                    var saveSelectedItems = Properties.Settings.Default.SelectedTypes.Split(',');
                        var Finditems = items.Where(i =>saveSelectedItems.Contains(i[lstObjectTypes.SelectedValuePath].ToString()));
                    Finditems.All(f =>
                    {
                        lstObjectTypes.SelectedItems.Add(f);
                        return true;
                    });
                }
                if (!string.IsNullOrEmpty(Properties.Settings.Default.SelectedOwners))
                {
                    lstOwnerList.SelectedItems.Clear();
                    var items = lstOwnerList.Items.OfType<DataRowView>();
                    var saveSelectedItems = Properties.Settings.Default.SelectedOwners.Split(',');
                    var findItems = items.Where(i => saveSelectedItems.Contains(i[lstOwnerList.SelectedValuePath].ToString()));
                    findItems.All(f => {
                        lstOwnerList.SelectedItems.Add(f); return true;
                    });
                }
                
            }
        }
        private void OwnerListLoad(DataTable table)
        {
            //string LikeOwner = "";
            //foreach (var filter in GetOwnerFilterFromSettings())
            //    if (!string.IsNullOrEmpty(filter) && filter != "%" && filter != "%%")
            //        LikeOwner += string.Format("OR OWNER LIKE '{0}' ", filter.ToUpper());
            //LikeOwner = LikeOwner.TrimStart('O').TrimStart('R');
            //if (!string.IsNullOrEmpty(LikeOwner))
            //{

            //    table = table.Select(LikeOwner).CopyToDataTable();
            //}


            lstOwnerList.ItemsSource = table.DefaultView;
        }
        /// <summary>
        /// Get bunldle from DB
        /// </summary>
        /// <param name="server"></param>
        /// <param name="progressPanel"></param>
        /// <param name="progressbar"></param>
        /// <param name="OnCompleted"> Action run on complete </param>
        private void GetBundle(LoginControlExpander server, Action OnCompleted)
        {

            StackPanel progressPanel = Progress1;
            ProgressBar progressbar = prgForServer1;
            if (server == SecondServer)
            {
                progressPanel = Progress2;
                progressbar = prgForServer2;
            }
            if (server.IsConnected)
            {
                progressPanel.Visibility=Visibility.Visible;
                var selTypes = lstObjectTypes.GetSelectedValue;
                var selOwners = lstOwnerList.GetSelectedValue;
                //DDL object info
                DDLBundleFromDBInfo DDLInfoFromDB = new DDLBundleFromDBInfo();
                DDLInfoFromDB.BundleFrom = server.ServerName;
                DDLInfoFromDB.ObjectTypes.AddRange(selTypes);
                DDLInfoFromDB.ObjectNameFilter = Properties.Settings.Default.ObjectNameFilter;
                DDLInfoFromDB.Owners.AddRange(selOwners);

                //server 1
                var  DDLBundle1= new DDLBundle();
                DDLBundle1 = new DDLBundle(DDLInfoFromDB);
                DDLOperation OperationLoad=new DDLOperation(DDLBundle1);

                OperationLoad.OnProgressChanged += (s, e) =>
                {
                    progressbar.Maximum = e.MaxProgress;
                    progressbar.Value = e.ProgressPercentage;
                    Debug.WriteLine(progressbar.Name+": " + e.MaxProgress.ToString() + "-" + e.ProgressPercentage.ToString());
                }; ;
                //  Bundle_OnProgressChanged;

                OperationLoad.OnLoadCompleted += (bun, arg) =>
                {

                    //grpResult.Visibility = Visibility.Visible;
                    //grpCompareBundle.Visibility = Visibility.Visible;
                    if (OnCompleted != null)
                        OnCompleted();
                   
                    // CompareBundles_Click(null, null);

                    //         Progress1.Visibility = Visibility.Hidden;
                };
                // read from database
                DBRetrieve dbRetrieve = new DBRetrieve(server.Conn);

                OperationLoad.Start(dbRetrieve);
                //DDLBundle1.Loader(dbRetrieve);
                DDLBandles[server.ServerName] = DDLBundle1;
            }
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            isAutoRunCompare = false;
            isCompleted1 = false;
            isCompleted2 = false;
           if (MainServer.IsConnected)
            {
                GetBundle(MainServer ,new Action(()=> {
                    isCompleted1 = true;
                }));
                GetBundle(SecondServer, new Action(() => {
                    isCompleted2 = true;
                    
                }));
              

            }
            else
            {
                MessageBox.Show("Main server is`t connected!!!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            while (true)
            {
                if (isCompleted1 && isCompleted2) break;
                Thread.Sleep(100);
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                                                  new Action(delegate { }));
            }
            //MessageBox.Show("Get completed");
        }


      
        /// <summary>
        /// Save bunlde 
        /// </summary>
        /// <param name="bundle"></param>
        /// <param name="server"></param>
        private void SaveBundle(DDLBundle bundle, LoginControlExpander server,Action OnCompleted)
        {
            if (bundle == null || bundle.DDLObjects.Count == 0) return;
            StackPanel pnlProgress= Progress1;
            ProgressBar progress =prgForServer1;
           if (server.ServerName==SecondServer.ServerName)
            {
                pnlProgress = Progress2;
                progress = prgForServer2;
            }
           
            
            pnlProgress.Visibility = Visibility.Visible;
            progress.Value = 0;
            progress.Maximum = bundle.DDLObjects.Count;
            ISaver saver = null;
            if (flgSaveToDir.IsChecked.Value == true)
            {
                saver = new SaveToDir(Properties.Settings.Default.OutputBunldeDir, server.ServerName);
            }
            else if (flgSaveToGit.IsChecked.Value)
            {
                GitInfo info = new GitInfo();
                info.LocalPath = System.IO.Path.Combine(Properties.Settings.Default.GITLocalPath, server.ServerName);
                //    info.Url = Properties.Settings.Default.GitUrl;
                info.UserName = Properties.Settings.Default.GitUser;
                info.UserEmail = Properties.Settings.Default.GitUserEmail;
                info.UserPass = Properties.Settings.Default.GitUser;
                info.ServerName = server.ServerName;
                saver = new BundleToGit(info);
                progress.Maximum = saver.MaxProgress;
            }
            else
            {
                throw new Exception("ISaver is undefined");
            }
            DDLOperation OperationSave = new DDLOperation(bundle);
            

            //bundle.SaveTo(saver);
            OperationSave.OnProgressChanged += (s, arg) =>
            {
                progress.Value = arg.ProgressPercentage;
            };
            OperationSave.OnLoadCompleted += (s, arg) =>
            {
                
                progress.Value = saver.MaxProgress;
                if (OnCompleted != null)
                    OnCompleted();
            };

            OperationSave.Start(saver);
        } 
        private void btnSaveAll_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.OutputBunldeDir))
            {
                MessageBox.Show("Export dir is`t be empty");
                return;
            }
            Button btn = sender as Button;
            grpResult.IsEnabled = false;
            grpCompareBundle.IsEnabled = false;
       
                btnGet_Click(null, null);
            grpResult.IsEnabled = true;
            if ((btn.Name == "btnSave1" || btn.Name == "btnSaveAll") && DDLBandles[MainServer.ServerName] != null)
            {
                btnSave1.IsEnabled = btnSaveAll.IsEnabled = false;
                    
                SaveBundle(DDLBandles[MainServer.ServerName], MainServer, new Action(() => {
                    btnSave1.IsEnabled = btnSaveAll.IsEnabled = true;
                }));
            }
            if ((btn.Name == "btnSave2" || btn.Name == "btnSaveAll") && DDLBandles[SecondServer.ServerName] != null)
            {
                btnSave2.IsEnabled = btnSaveAll.IsEnabled = false;
                SaveBundle(DDLBandles[SecondServer.ServerName], SecondServer, new Action(() => {
                    btnSave2.IsEnabled = btnSaveAll.IsEnabled = true;
                }));
            }
            //while(true)
            //{
            //    if (btnSave1.IsEnabled && btnSave2.IsEnabled) break;
            //    Thread.Sleep(100);
            //   // Utils.DoEvents();
            //}
        

                grpCompareBundle.IsEnabled = true ;
         
}



      
        private void CompareBundles_Click(object sender, RoutedEventArgs e)
        {
            btnGet_Click(null, null);
            if ((!isAutoRunCompare  && isCompleted1 && isCompleted2) || sender!=null)
            {

                MainWindow mainwin = Window.GetWindow(this) as MainWindow;
                if (mainwin != null)
                {
                    CompareBundles CompareWindow = new UserControls.CompareBundles(DDLBandles[MainServer.ServerName], DDLBandles[SecondServer.ServerName]);
                    mainwin.tabCompare.Content = CompareWindow;
                    mainwin.tabCompare.Focus();

                }
            }

        }

        

        public  void SaveSettingToXML()
        {
            BundleCommand commandServer1 = new BundleCommand();
            commandServer1.Retrieve.RetrieveType = BundleType.DB;
            commandServer1.Save.SaveType = BundleType.Directory;
            commandServer1.Retrieve.BundleFrom = MainServer.ServerName;
            commandServer1.Retrieve.Login = MainServer.Login;
            commandServer1.Retrieve.Password = MainServer.GetPassword();
            
            
            commandServer1.Retrieve.ObjectNameFilter = Properties.Settings.Default.ObjectNameFilter;
            commandServer1.Retrieve.ObjectTypes = lstObjectTypes.GetSelectedValue;
            commandServer1.Retrieve.ObjectOwners = lstOwnerList.GetSelectedValue;
            //server2
            BundleCommand commandServer2 = null;
            if (SecondServer.IsConnected)
            {

                 commandServer2 = commandServer1.Clone() as BundleCommand;
                commandServer2.Retrieve.BundleFrom = SecondServer.ServerName;
                if (!chkUserLogin.IsChecked.Value)
                {
                    commandServer2.Retrieve.Login = SecondServer.Login;
                    commandServer2.Retrieve.Password = SecondServer.GetPassword();

                }
            }
            ConsoleCommand commands = new ConsoleCommand();
            commands.Commands.Add(commandServer1);
            if (commandServer2!=null)
                commands.Commands.Add(commandServer2);
            using (System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog())
            {
                dialog.Filter = "XML file (*.xml)|*.xml";
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    commands.Serialize<ConsoleCommand>(dialog.FileName);
                }
            }
        }


        private void btnLoadOwner_Click(object sender, RoutedEventArgs e)
        {
           
            PKG_API api = new PKG_API(MainServer.Conn);
            
            DataTable ownersFilterd = api.GETOWNERLIST_BYFLT(txtOwnerFlt.Text);
            OwnerListLoad(ownersFilterd);
            lstOwnerList.SelectAll();

        }
    }
}
