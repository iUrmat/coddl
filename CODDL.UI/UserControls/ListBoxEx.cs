﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace CODDL.UI.UserControls
{
    public class ListBoxEx:ListBox
    {
        /// <summary>
        /// Get list of selected value
        /// </summary>
        public List<string> GetSelectedValue
        {
            get
            {
                List<string> selectedList = new List<string>();
                if (SelectedItems == null || SelectedItems.Count == 0) return selectedList;

                foreach (DataRowView selectitem in SelectedItems)
                {
                    selectedList.Add(selectitem[this.SelectedValuePath] as string);
                 }

                return selectedList;
             }
            }
    }
}
