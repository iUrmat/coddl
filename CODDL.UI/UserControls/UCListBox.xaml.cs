﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CODDL.UI.UserControls
{
    /// <summary>
    /// Interaction logic for UCListBox.xaml
    /// </summary>
    public partial class UCListBox : UserControl
    {
        public UCListBox()
        {
            InitializeComponent();
        }

        private void SelectAll(object sender, RoutedEventArgs e)
        {
            lstObjectTypes.SelectAll();
        }

        private void DeSelectAll(object sender, RoutedEventArgs e)
        {
            lstObjectTypes.SelectedItems.Clear();
        }
        public List<string> GetSelectedValue
        {
            get
            {
                return lstObjectTypes.GetSelectedValue;
            }
        }
        public string SelectedValuePath
        {
            get { return lstObjectTypes.SelectedValuePath; }
            set { lstObjectTypes.SelectedValuePath = value; }
        }
        public string DisplayMemberPath
        {
            get { return lstObjectTypes.DisplayMemberPath; }
            set { lstObjectTypes.DisplayMemberPath = value; }
        }
                
        public IEnumerable ItemsSource
        {
            get { return lstObjectTypes.ItemsSource; }
            set
            {
                lstObjectTypes.ItemsSource = value;
                
            }
        }
        public void SelectAll()
        {
            lstObjectTypes.SelectAll();
        }       
        public ItemCollection Items
        {
            get { return lstObjectTypes.Items;  }
        }
        public IList SelectedItems
        {
            get { return lstObjectTypes.SelectedItems; }
        }
    }
}
