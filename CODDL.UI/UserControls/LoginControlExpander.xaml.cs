﻿
using CODDL.API.DB;
using CODDL.UI.Helpers;
using CODDL.UI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CODDL.API.Model;

namespace CODDL.UI.UserControls
{
    /// <summary>
    /// Interaction logic for LoginControl.xaml
    /// </summary>
    public partial class LoginControlExpander : UserControl,ILoadWizard
    {
        public Oracle.ManagedDataAccess.Client.OracleConnection Conn;

        public ProgressBar ProgressBar { get; set; }
        public event EventHandler<LoadWizardArgs> OnLoadWizard;
        public event EventHandler<LoginEventArgs> OnLogin;

        public string ServerName { get; set; }
        public string Login { get; set; }
        public bool IsConnected
        {
            get
            {
                return Conn != null && Conn.State == System.Data.ConnectionState.Open;
            }
        }
        public bool AutoLogin { get; set; }
        #region Properties
        public static readonly DependencyProperty IsExpandedProperty =
        DependencyProperty.Register("IsExpanded", typeof(bool), typeof(LoginControlExpander), new UIPropertyMetadata(false
            , IsExpanedChangeCallBack));

        private static void IsExpanedChangeCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LoginControlExpander loginControl = d as LoginControlExpander;
            loginControl.expander.IsExpanded = (e.NewValue as bool?) ?? false; 
            
        }
        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set { SetValue(IsExpandedProperty, value); }
        }
        public static readonly DependencyProperty ServerLabelProperty =
        DependencyProperty.Register("ServerLabel", typeof(string), typeof(LoginControlExpander),new UIPropertyMetadata(string.Empty,serverNameChangeCallBack));
        private static void serverNameChangeCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            LoginControlExpander loginControl = d as LoginControlExpander;
            loginControl.lblServer.Content = e.NewValue as string;
        }
        public string ServerLabel
        {
            set
            {
                SetValue(ServerLabelProperty, value);
                lblServer.Content = value;
            }
            get
            {
                return (string)GetValue(ServerLabelProperty);
            }
        }
        #endregion
        
        public LoginControlExpander()
        {
            InitializeComponent();
            ServerName = "KRDEV12";
            Login = "CODDL";
            DataContext = this;
            
        }
        ~LoginControlExpander()
        {
           if (Conn!=null)
                Conn.Close();
        }
        internal string GetPassword()
        {
            return this.psPass.Password;
            
        }
        public void ClickLogin()
        {
            btnConn_Click(btnConn, new RoutedEventArgs());
        }
        private void btnConn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Conn = OraConnector.Instance.connect(Login, this.psPass.Password, ServerName);
            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Conn = null;
            }
            finally
            {
                if (OnLogin!=null)
                {
                    LoginEventArgs args = new LoginEventArgs();
                    args.Conn = Conn;
                    OnLogin(this, args);

                }
            }
        }

        public DDLBundle Load()
        {
            DDLBundle result = new DDLBundle();
            if (OnLoadWizard != null)
            {
                LoadWizardArgs args = new LoadWizardArgs();
                args.ddlBundle = result;
                OnLoadWizard(this, args);
                result = args.ddlBundle;
            }
            return result;
        }
    }
}
