﻿using CODDL.API;
using CODDL.API.Model;

using CODDL.UI.Helpers;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CODDL.UI.UserControls
{
    /// <summary>
    /// Interaction logic for CompareBundles.xaml
    /// </summary>
    public partial class CompareBundles : UserControl
    {
        private DDLBundle DDLBundle1;
        private DDLBundle DDLBundle2;
        private ObservableCollection<Difference> DifferenceList = new ObservableCollection<Difference>();
        public bool IsBundlesOK
        {
            get { return DDLBundle1 != null && DDLBundle2.DDLObjects.Count > 0 && DDLBundle2 != null && DDLBundle2.DDLObjects.Count > 0; }
        }
        public CompareBundles(DDLBundle bundle1=null,DDLBundle bundle2=null)
        {
            DDLBundle1 = bundle1;
            DDLBundle2 = bundle2;
            InitializeComponent();
            this.DataContext = this;
            
        }
        protected override void OnInitialized(EventArgs e)
        {
            
            base.OnInitialized(e);
            LoadSettings();
            btnCompare_Click(null, null);
        }
        private void LoadSettings() {
            txtCompareViewer.Text = Properties.Settings.Default.CompareViewer;
            flgIgnoreCase.IsChecked = Properties.Settings.Default.IgnoreCase;
            flgWhiteSpace.IsChecked = Properties.Settings.Default.IgnoreWhiteSpace;
        }
        void SaveSetting()
        {
            Properties.Settings.Default.IgnoreWhiteSpace = flgWhiteSpace.IsChecked.Value;
            Properties.Settings.Default.IgnoreCase = flgIgnoreCase.IsChecked.Value;
            Properties.Settings.Default.CompareViewer = txtCompareViewer.Text;
            Properties.Settings.Default.Save();
            
        }
        private void btnCompare_Click(object sender, RoutedEventArgs e)
        {
            BundleCompare comparer = new BundleCompare(flgWhiteSpace.IsChecked.Value, flgIgnoreCase.IsChecked.Value);
            DifferenceList= comparer.Compare(DDLBundle1, DDLBundle2);
            grdDifference.DataContext = DifferenceList;
        }

        private void Browser_Click(object sender, RoutedEventArgs e)
        {
            using (System.Windows.Forms.OpenFileDialog dlgFile = new System.Windows.Forms.OpenFileDialog())
            {
                dlgFile.Filter="Execute file (*.exe)|*.exe";
                if (dlgFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    txtCompareViewer.Text = dlgFile.FileName;
                    Properties.Settings.Default.CompareViewer= txtCompareViewer.Text;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void DiffView_Click(object sender, RoutedEventArgs e)
        {
            Difference selected = grdDifference.SelectedItem as Difference;
            if (selected == null) return;
            string TempPath= System.IO.Path.GetTempPath();
            string Bundle1File = selected.SaveToBundle1(TempPath);
            TempPath = System.IO.Directory.CreateDirectory(System.IO.Path.Combine(TempPath, "cmp")).FullName;
            string Bundle2File = selected.SaveToBundle2(TempPath);
            if (!System.IO.File.Exists(txtCompareViewer.Text))
            {
                MessageBox.Show("Coudln't find compare utility " + txtCompareViewer.Text);
            }
            else
            {
                Process.Start(txtCompareViewer.Text, "\"" + Bundle1File + "\" \"" + Bundle2File + "\"");
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            using (System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog())
            {
                if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                string SelectedFolder = dlg.SelectedPath;
                var SelectedBundles = DifferenceList.Where(d => d.IsChecked).ToList();
                if (SelectedBundles == null || SelectedBundles.Count() == 0) {
                    SelectedBundles = new List<Difference>();
                     SelectedBundles.Add(grdDifference.SelectedItem as Difference);
                }
                switch (btn.Name)
                {
                    case "btnSaveBundle1":
                        foreach (Difference dif in SelectedBundles)
                        {
                            dif.SaveToBundle1(SelectedFolder);
                        }
                        break;
                    case "btnSaveBundle2":
                        foreach (Difference dif in SelectedBundles)
                        {
                            dif.SaveToBundle2(SelectedFolder);
                        }
                        break;

                }
                
            }
        }

        private void CheckAll_Checked(object sender, RoutedEventArgs e)
        {
            foreach (Difference diff in DifferenceList)
                diff.IsChecked = true;
        }

        private void CheckAll_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (Difference diff in DifferenceList)
                diff.IsChecked = false;

        }

        private void grdDifference_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DiffView_Click(sender, null);
        }

        private void flgWhiteSpace_Click(object sender, RoutedEventArgs e)
        {
            SaveSetting();
        }
    }
}
