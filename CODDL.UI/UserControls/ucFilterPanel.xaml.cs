﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CODDL.UI.UserControls
{
    /// <summary>
    /// Interaction logic for ucFilterPanel.xaml
    /// </summary>
    public partial class ucFilterPanel : UserControl
    {
        public event EventHandler OnClickFill;
        public LoginControlExpander LoginControl
        {
            get;private set;
        }
        public ucFilterPanel(LoginControlExpander logincontrol)
        {
            InitializeComponent();
            LoginControl = logincontrol;
      //      lstObjectTypes.ItemsSource = tableType.DefaultView;
        //    lstOwnerList.ItemsSource = tableOwner.DefaultView;
            btnLoadOwner.Click += BtnLoadOwner_Click;
        }

        private void BtnLoadOwner_Click(object sender, RoutedEventArgs e)
        {
            if (OnClickFill != null)
                OnClickFill(sender,e);
        }
        public void OnClick()
        {
            BtnLoadOwner_Click(null, null);
        }
    }
}
