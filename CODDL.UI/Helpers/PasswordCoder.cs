﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CODDL.UI.Helpers
{
    public class PasswordCoder
    {
        
        public static string EncryptText(string openText)
        {
            // Get the key and IV.
           
            RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
            byte[] key = ASCIIEncoding.ASCII.GetBytes("Karavay2015");
            byte[] IV = ASCIIEncoding.ASCII.GetBytes("Karavay2015");
            ICryptoTransform encryptor = rc2CSP.CreateEncryptor(key,IV);
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    byte[] toEncrypt = Encoding.Unicode.GetBytes(openText);

                    csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
                    csEncrypt.FlushFinalBlock();

                    byte[] encrypted = msEncrypt.ToArray();

                    return Convert.ToBase64String(encrypted);
                }
            }
        }

        public static string DecryptText(string encryptedText)
        {
            if (string.IsNullOrEmpty(encryptedText)) return "";
            RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
            byte[] key = ASCIIEncoding.ASCII.GetBytes("Karavay2015");
            byte[] IV = ASCIIEncoding.ASCII.GetBytes("Karavay2015");
            ICryptoTransform decryptor = rc2CSP.CreateDecryptor(key, IV);
            using (MemoryStream msDecrypt = new MemoryStream(Convert.FromBase64String(encryptedText)))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    List<Byte> bytes = new List<byte>();
                    int b;
                    do
                    {
                        b = csDecrypt.ReadByte();
                        if (b != -1)
                        {
                            bytes.Add(Convert.ToByte(b));
                        }

                    }
                    while (b != -1);

                    return Encoding.Unicode.GetString(bytes.ToArray());
                }
            }
        }
    }
}
