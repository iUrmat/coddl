﻿using CODDL.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CODDL.UI.Helpers
{
    public class LoadWizardArgs: EventArgs
    {
        public DDLBundle ddlBundle { get; set; }
    }
}
